import jwt from "jsonwebtoken"
import config from '../config/config'

export const verifyAccessToken = function(token) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, config.accessTokenSecret, (err, user) => {
        if (err) {
          reject(err);
        }
        resolve(user);
    })
  })
}

export const generateAccessToken = function(payload) {
  return jwt.sign(payload, config.accessTokenSecret, { expiresIn: '24h' });
}