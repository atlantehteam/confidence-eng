import _ from 'lodash';
import AccessControl from 'accesscontrol';
import BusinessError, {HttpStatus} from '../BusinessError';

const defaultQueryFields = ['id', 'createdAt', 'updatedAt', 'tenantId']

const ac = new AccessControl();

const chapterFields = ['name', 'level', 'comments', 'content', 'vocabulary', 'summaryPDF', 'vocabularyPDF', 'downloads', 'order'];
const chapterReadOnlyFields = ['goldenQuestionsCount', 'mcQuestionsCount'];
const chapterStudentReadOnlyFields = ['name', 'level', 'content', 'vocabulary', 'summaryPDF', 'vocabularyPDF', 'downloads', 'order']
ac.grant(['superAdmin'])
    .readOwn('chapter', [...chapterFields, ...chapterReadOnlyFields, ...defaultQueryFields])
    .createOwn('chapter', chapterFields)
    .updateOwn('chapter', chapterFields)
    .deleteOwn('chapter')
ac.grant(['student'])
    .readOwn('chapter', [...chapterStudentReadOnlyFields, ...defaultQueryFields])

const userFields = ['name', 'email', 'permissions', 'selectedLevel'];
const userAdminFields = ['generatePassword'];
const userReadOnlyFields = ['role'];
ac.grant(['superAdmin'])
    .readOwn('user', [...userFields, ...userReadOnlyFields, ...defaultQueryFields])
    .createOwn('user', [...userFields, ...userAdminFields])
    .updateOwn('user', [...userFields, ...userAdminFields])
    .deleteOwn('user')

const userSettingsFields = ['name', 'selectedLevel'];
const userSettingsReadOnlyFields = ['name', 'email', 'permissions', 'selectedLevel', 'primaryRole'];
ac.grant(['superAdmin', 'student'])
    .readOwn('userSettings', [...userSettingsFields, ...userSettingsReadOnlyFields, ...defaultQueryFields])
    .updateOwn('userSettings', [...userSettingsFields])

const unitFields = [];
const unitReadOnlyFields = ['content'];
ac.grant(['superAdmin', 'student'])
    .readOwn('btw', [...unitFields, ...unitReadOnlyFields, ...defaultQueryFields])
    .readOwn('words', [...unitFields, ...unitReadOnlyFields, ...defaultQueryFields])
    .readOwn('smallTalk', [...unitFields, ...unitReadOnlyFields, ...defaultQueryFields])

const goldenQuestionsFields = ['title', 'answer', 'order', 'answerAudio'];
const goldenQuestionsReadOnlyFields = ['chapterId'];
ac.grant(['superAdmin'])
    .readOwn('goldenQuestion', [...goldenQuestionsFields, ...goldenQuestionsReadOnlyFields, ...defaultQueryFields])
    .createOwn('goldenQuestion', [...goldenQuestionsFields])
    .updateOwn('goldenQuestion', [...goldenQuestionsFields])
    .deleteOwn('goldenQuestion')
ac.grant(['student'])
    .readOwn('goldenQuestion', [...goldenQuestionsFields, ...goldenQuestionsReadOnlyFields, ...defaultQueryFields])

const mcQuestionsFields = ['title', 'answers', 'order'];
const mcQuestionsReadOnlyFields = ['chapterId'];
ac.grant(['superAdmin'])
    .readOwn('mcQuestion', [...mcQuestionsFields, ...mcQuestionsReadOnlyFields, ...defaultQueryFields])
    .createOwn('mcQuestion', [...mcQuestionsFields])
    .updateOwn('mcQuestion', [...mcQuestionsFields])
    .deleteOwn('mcQuestion')
ac.grant(['student'])
    .readOwn('mcQuestion', [...mcQuestionsFields, ...mcQuestionsReadOnlyFields, ...defaultQueryFields])

ac.lock()

function getRoleByEntityId(resource, user, entityName, path = `${entityName}Id`) {
    if (!_.get(resource, path) || _.isEmpty(user.roles, entityName)) {
        return null;
    }
    return _.get(_.find(user.roles[entityName], l => String(l.entityId) === String(resource[path])), 'role');
}

function getRoleByOwnId(resource, user, entityName) {
    return getRoleByEntityId(resource, user, entityName, 'id')
}

function getUserPrimaryRole(user) {
    return user.primaryRole;
}

function getUserRoleByTenant(resource, user) {
    return (user.tenantId === resource?.tenantId) && getUserPrimaryRole(user);
}

function getUserRoleByPermission(resource, user) {
    return (user.permissions?.includes(resource)) && getUserPrimaryRole(user);
}

const superAdminRoles = ['superAdmin'];
function getRoleForResource(resourceName, resource, user) {
    if (!user) {return null;}
    
    const role = getUserRoleByTenant(resource, user);
    if (superAdminRoles.includes(role)) {return role;}

    switch (resourceName) {
    case 'chapter':
        return getUserRoleByTenant(resource, user)
    case 'btw':
    case 'words':
    case 'smallTalk':
        return getUserRoleByPermission(resource, user)
    case 'goldenQuestion':
    case 'mcQuestion':
        return getUserRoleByTenant(resource, user) && getUserRoleByPermission('core', user)
    case 'cronJob':
    case 'userSettings':
        return getUserPrimaryRole(user);
    default:
        throw new Error(`Invalid resource name: ${resourceName}`);
    }
}

const authorizeOperation = operation => function(resourceName, resource, req) {
    try {
        const role = getRoleForResource(resourceName, resource, req.user);
        const finalRole = role || (req.user ? 'authenticated' : 'public');
        if (ac.hasRole(finalRole)) {
            const policy = ac.can(finalRole)[`${operation}Own`](resourceName);
            if (policy.granted) {
                return policy;
            }
        }
        console.log(`Unauthorized access trying to ${operation} resource=${resourceName} with id(${resource.id}). User (id=${_.get(req.user.id)}, email=${_.get(req.user.email)}, primaryRole=${_.get(req.user.primaryRole)}) with role ${role}`);
    } catch (err) {
        console.error(err);
    }
    throw new BusinessError(HttpStatus.FORBIDDEN);
};

export const authorize = {
    read: authorizeOperation('read'),
    create: authorizeOperation('create'),
    update: authorizeOperation('update'),
    delete: authorizeOperation('delete'),
}

const enforceEntityByTenantId = entity => user => ({[entity]: {tenantId: user.tenantId}})
const authorizationMap = {
    chapter: {superAdmin: enforceEntityByTenantId('chapter'), student: enforceEntityByTenantId('chapter')},
    goldenQuestion: {superAdmin: enforceEntityByTenantId('goldenQuestion'), student: enforceEntityByTenantId('goldenQuestion')},
    mcQuestion: {superAdmin: enforceEntityByTenantId('mcQuestion'), student: enforceEntityByTenantId('mcQuestion')},
    user: {superAdmin: enforceEntityByTenantId('user')},
}
export const enforceAuthorization = function(entityType, req, context) {
    const {user} = req;
    if (!user) {
        throw new BusinessError(HttpStatus.FORBIDDEN);
    }

    const entityAuthorization = _.get(authorizationMap, [entityType, user.primaryRole]);
    if (!entityAuthorization) {
        console.error(`Can't enforceAuthorization: Missing entityAuthorization for entityType: ${entityType}, and role: ${user.primaryRole}`);
        throw new BusinessError(HttpStatus.FORBIDDEN);
    }
    
    return entityAuthorization(user, context)
}