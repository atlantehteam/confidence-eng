export const permissionsOptions = {
	core: 'Core',
	byTheWay: 'By The Way',
	words: '5k Words',
	smallTalk: 'Small Talk',
	emailHelper: 'Email Helper',
}

export const permissionKeys = Object.keys(permissionsOptions);
