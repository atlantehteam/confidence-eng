import _ from 'lodash'
import knex from '../services/knex'

function resolveTemplate(template, values) {
    const result = Object.keys(values).reduce((aggr, key) => {
        return aggr.replace(new RegExp(`{{${key}}}`, 'g'), values[key] || '')
    }, template);
    return result;
}



export const generateMessageTemplate = function(message, options) {
    switch (message.kind) {
        default:
            throw new Error(`Invalid message kind: ${message.kind} on message ${message.id}`);
    }
}
