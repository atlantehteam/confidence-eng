import {Storage} from '@google-cloud/storage';
import HttpStatus from 'http-status-codes'
import BusinessError from "./BusinessError"
import fs from 'fs-extra'
import path from 'path'
import config from '../config/config'

const storage = new Storage();

const pdfContentTypes = {
    'application/pdf': 'pdf',
}
const mp3ContentTypes = {
    'audio/mp3': 'mp3',
    'audio/mpeg': 'mp3',
}
const imgContentTypes = {
    'image/jpg': 'jpg',
    'image/jpeg': 'jpeg',
    'image/png': 'png',
}
const mediaContentTypes = {...pdfContentTypes, ...imgContentTypes, ...mp3ContentTypes};

export const stripBase64FilePrefix = (file) => {
    const strippedFile = file.slice(file.indexOf(',') + 1); //remove data:image/jpeg;base64,
    return strippedFile;
}

export const getContentType = (file) => {
    const typeEnd = file.indexOf(';');
    const type = file.slice(5, typeEnd);
    return type.toLowerCase();
}

const covertFileToBuffer = (file) => {
    const strippedFile = stripBase64FilePrefix(file);
    const buffer = Buffer.from(strippedFile, 'base64');
    return buffer;
}

const isImage = (type) => {
    return Boolean(imgContentTypes[type]);
}

const publicToAbsolutePath = (relativePath) => {
    return path.resolve(config.mediaDir.absolutePath, path.relative(config.mediaDir.absolutePath, relativePath))
}

const toPublicDirectory = (relative) => {
    return path.normalize(`${config.mediaDir.public}/${relative}`);
}

export const duplicateMedia = async (fromPublic, toRelativeFolder) => {
    const fromAbsolute = publicToAbsolutePath(fromPublic);
    const toAbsolute = publicToAbsolutePath(toPublicDirectory(toRelativeFolder));
    const fileName = path.basename(fromAbsolute)
    await fs.copy(fromAbsolute, `${toAbsolute}/${fileName}`, {recursive: true})
    const relative = path.relative(config.mediaDir.absolutePath, path.resolve(config.mediaDir.absolutePath, toRelativeFolder, fileName))
    return toPublicDirectory(relative);
}

const uploadMediaFile = async (bucket, filePath, mediaBuffer, contentType, {output} = {}) => {
    const file = storage.bucket(bucket).file(filePath)
    await file.save(mediaBuffer, {contentType})
    const result = {
        path: file.id,
        gsPath: `gs://${bucket}/${filePath}`,
    };
    return output ? result[output] : result;
}

export const uploadMedia = async (bucket, path, file, fileName, {format, output}) => {
    if (!file) {
        console.error(new Error('file cannot be null'));
        return Promise.reject(new BusinessError(HttpStatus.BAD_REQUEST));
    }

    if (!file.startsWith('data:')) {
        if (output === 'gsPath') {
            const {groups} = file.match(new RegExp(`https://storage.googleapis.com/(?<bucket>[^/]+)/(?<path>.*?)\\?`)) || []
            if (!groups || !groups.bucket || !groups.path) {
                throw new Error(`gsPath not supported with non storage item: ${file}`)
            }
            return {[fileName]: `gs://${bucket}/${groups.path}`}
        }
        if (output !== 'path') {
            throw new Error('non base64 formatted file is only allowed when output=path')
        }
        return Promise.resolve({[fileName]: file});
    }
    if (!fileName) {
        console.error(new Error('fileName cannot be null'));
        return Promise.reject(new BusinessError(HttpStatus.BAD_REQUEST));
    }

    const contentType = getContentType(file);

    const ext = mediaContentTypes[contentType];

    switch (format) {
    case 'img':
        if (!isImage(contentType)) {
            throw new BusinessError(HttpStatus.BAD_REQUEST, 'MEDIA_ONLY_IMAGE');
        }
        break;
    case 'pdf':
        if (!pdfContentTypes[contentType]) {
            throw new BusinessError(HttpStatus.BAD_REQUEST, 'PDF_ONLY_IMAGE');
        }
        break;
    case 'mp3':
        if (!mp3ContentTypes[contentType]) {
            throw new BusinessError(HttpStatus.BAD_REQUEST, 'PDF_ONLY_IMAGE');
        }
        break;
    default:
        if (format) {
            throw new Error(`Unkown format ${format}`)
        }
    }

    if (!ext) {
        console.error(new Error(`ContentType ${contentType} is not supported`));
        throw new BusinessError(HttpStatus.BAD_REQUEST);
    }

    const buffer = covertFileToBuffer(file);
    const result = await uploadMediaFile(bucket, `${path}/${fileName}.${ext}`, buffer, contentType, {output});
    return {[fileName]: result}
}

export const uploadMediaOrResolve = async (bucket, bucketPath, mediaFile, propName, {format, output}) => {
    if (mediaFile === undefined) {
        return undefined;
    }
    if (mediaFile === null) {
        if (output) {
            return {[propName]: null};
        }
        return {[propName]: {url: null, bucket: null, path: null}};
    }
    if (!bucketPath) {
        throw new Error('Missing bucketPath');
    }
    const result = await uploadMedia(bucket, bucketPath, mediaFile, propName, {format, output})
    return result;
}
