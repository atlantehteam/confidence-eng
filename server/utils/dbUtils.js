import knex from '../services/knex'
import _ from 'lodash'

const operators = ['$and', '$or'];

export const mapToDBKey = function(string, map) {
    const stringParts = string.split('.');
    if (stringParts.length !== 2) {
        throw new Error(`mapToDbKey: Invalid stringParts length: ${stringParts.length}`);
    }
    const keyPrefix = map[stringParts[0]];
    if (!keyPrefix) {
        throw new Error(`mapToDbKey: Key ${stringParts[0]} is missing from field map`);
    }
    return `${keyPrefix}.${stringParts[1]}`
}
export const mapToDBFields = function(fields, map) {
    if (!fields) {return null}

    if (_.isArray(fields)) {
        return fields.map(f => mapToDBFields(f, map));
    }
    return Object.keys(fields).reduce((aggr, key) => {
        if (operators.includes(key)) {
            aggr[key] = mapToDBFields(fields[key], map)
            return aggr;
        }
        const keyPrefix = map[key];
        if (!keyPrefix) {
            throw new Error(`Key ${key} is missing from field map`);
        }
        if (!fields[key]) {return aggr;}

        const keyFields = Object.keys(fields[key]);
        keyFields.forEach(field => {
            aggr[`${keyPrefix}.${field}`] = fields[key][field];
        })
        return aggr;
    }, {});
}

// TODO: validate operators
const sanitizeKeyValue = function(keyVal) {
    if (!_.isObject(keyVal) || keyVal instanceof Date) {
        return {op: '=', value: keyVal}
    }
    return keyVal;
}

const sanitizeKey = function(key, value) {
    const keyParts = key.split('.')
    if (keyParts.length < 3) {
        if (value.cast) {
            return knex.raw(`??::${value.cast}`, [key]);
        }
        return key;
    }
    
    const partWithoutTable = keyParts.slice(1)
    let keyQuery = partWithoutTable.reduce((agg, part, index) => {
        if (!index) {
            return `${agg}.??`;
        }
        return `${agg}->>?`
    }, '??');

    if (value.cast) {
        keyQuery += `::${value.cast}`;
    }
    return knex.raw(keyQuery, keyParts);
}

export const addWhereClause = function(query, where, conjunction) {
    if (!where) {
        return query;
    }
    const keys = Object.keys(where);
    if (where['$and']) {
        return query.where(builder => {
            const chilren = where['$and'];
            for (let i = 0; i < chilren.length; i++) {
                addWhereClause(builder, where['$and'][i], 'and');
            }
        })
    }
    if (where['$or']) {
        return query.where(builder => {
            const chilren = where['$or'];
            for (let i = 0; i < chilren.length; i++) {
                addWhereClause(builder, where['$or'][i], 'or');
            }
        })
    }
    const whereClause = keys.reduce((aggr, rawKey) => {
        const key = sanitizeKey(rawKey, where[rawKey]);
        const val = sanitizeKeyValue(where[rawKey]);
        switch (val.op) {
        case '=':
        case '>':
        case '<':
        case '>=':
        case '<=':
        case 'like':
        case 'ilike':
            if (conjunction === 'or') {
                return aggr.orWhere(key, val.op, val.value);
            }
            return aggr.where(key, val.op, val.value);
        case 'between':
            if (conjunction === 'or') {
                return aggr.orWhereBetween(key, [val.startVal, val.endVal]);
            }
            return aggr.whereBetween(key, [val.startVal, val.endVal]);
        case 'in':
            if (conjunction === 'or') {
                return aggr.orWhereIn(key, val.value);
            }
            return aggr.whereIn(key, val.value);
        case 'not':
            if (conjunction === 'or') {
                return aggr.orWhereNot(key, val.value);
            }
            return aggr.whereNot(key, val.value);
        case 'notNull':
            if (conjunction === 'or') {
                return aggr.orWhereNotNull(key);
            }
            return aggr.whereNotNull(key);
        default:
            throw new Error(`Invalid operator ${val.op}`);
        }
    }, query);
    return whereClause;
}

export const upsert = async function(knexInstance, table, toInsert, keysToUpdate, constraint) {
    const insert = knexInstance(table).insert(toInsert);
    const excludedObject = keysToUpdate.reduce((aggr, key) => {
        aggr[key] = knexInstance.raw(`EXCLUDED."${key}"`);
        return aggr;
    }, {});
    const update = knexInstance.queryBuilder().update(excludedObject);
    const res = await knexInstance.raw(`? ON CONFLICT ${constraint} DO ? returning *`, [insert, update])
    return res;
}