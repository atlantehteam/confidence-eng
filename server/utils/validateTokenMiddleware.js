import HttpStatus from 'http-status-codes'
import BusinessError from './BusinessError'
import {verifyAccessToken} from './jwt'
import * as internalModel from '../routes/models/internal';

const validateAccessToken = (errorWhenNull, role) => async (req, res, next) => {
    if ((!req.headers.authorization || !req.headers.authorization.startsWith('Bearer '))) {
        if (!errorWhenNull) {
            return next();
        }
        console.error(`No access_token was passed as a Bearer token in the Authorization header.
            Make sure you authorize your request by providing the following HTTP header:
            Authorization: Bearer <access_Token>
            or by passing a "__session" cookie.`);
        return next(new BusinessError(HttpStatus.UNAUTHORIZED));
    }

    let idToken;
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
        idToken = req.headers.authorization.split('Bearer ')[1];
    }
    try {
        const userPayload = await verifyAccessToken(idToken);
        if (role && userPayload.primaryRole !== role) {
            throw new Error(`Unauthorized access to role ${userPayload.primaryRole}. Only ${role} allowed`)
        }

        req.user = {...userPayload};

        return next();
    } catch (error) {
        console.error('Error while verifying Firebase ID token:');
        console.error(error);
        return next(new BusinessError(HttpStatus.UNAUTHORIZED));
    }
};

export const validateInternalApiKey = async function(req, res, next) {
    let apiKey;
    if (req.query.apiKey) {
        console.log(`apiKey passed in query`)
        apiKey = req.query.apiKey
    } else if (req.headers.authorization) {
        if (!req.headers.authorization.startsWith('APIKEY ')) {
            return next(new BusinessError(HttpStatus.UNAUTHORIZED, 'invalidApiKeyStructure', {message: 'apiKey be in the form of: APIKEY <YourApiKey>'}));
        }
        apiKey = req.headers.authorization.split('APIKEY ')[1]
    } else {
        return next(new BusinessError(HttpStatus.UNAUTHORIZED));
    }

    try {
        const validApiKey = await internalModel.getValidInternalApiKey(apiKey);
        if (!validApiKey) {
            throw new Error(`ApiKey is not valid: ${apiKey}`)
        }

        req.user = validApiKey;
        req.userRole = 'internalApi';

        return next();
    } catch (error) {
        console.error('Error while verifying internval api key');
        console.error(error);
        return next(new BusinessError(HttpStatus.UNAUTHORIZED));
    }
};

export default validateAccessToken(true);
export const validateRole = role => validateAccessToken(true, role);
export const decodeUserToken = validateAccessToken(false);
