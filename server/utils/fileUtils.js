import {Storage} from '@google-cloud/storage';
import config from '../config/config';

const storage = new Storage();

export const signFileUrl = async function(gsFileUrl, durationSeconds = config.gcs.defaultDurationSeconds) {
    const bucket = gsFileUrl.slice(5).match(/(.*?)\//)[1]
    if (!bucket) {
        throw new Error(`bucket not found. Invalid file url: ${gsFileUrl}`);
    }

    const fileName = gsFileUrl.match(`${bucket}/(.*)`)[1];
    const [signedUrl] = await storage
        .bucket(bucket)
        .file(fileName)
        .getSignedUrl({expires: Date.now() + durationSeconds * 1000, action: 'read', version: 'v2'})
    return signedUrl;
}

export async function signItemUrl(item) {
    const {sign, url, ...rest} = item;

    if (!sign) {return item}

    return {
        ...rest,
        url: await signFileUrl(url),
    }

}
export async function signUrls(items) {
    const signedItems = await Promise.all(items.map(signItemUrl))
    return signedItems;
}
