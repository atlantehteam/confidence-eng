import _ from "lodash";
import BusinessError from "./BusinessError";

export const routeWrapper = route => async (req, res, next) => {
    try {
        return await route(req, res);
    } catch (err) {
        return next(err)
    }
}

export const mapObject = (obj, mapping) => {
    return Object.entries(mapping).reduce((aggr, [adKey, modelKey]) => {
        aggr[modelKey] = obj[adKey];
        return aggr;
    }, {})
}

export const errorsMapper = (possibleErrors, mapper) => {
    const errorResult = possibleErrors.reduce((aggr, res, index) => {
        if (res.status === 'fulfilled') {
            return aggr;
        }
        console.error(res.reason);
        const err = res.reason instanceof BusinessError ? res.reason : new BusinessError()
        const content = mapper(index);
        return [...aggr, {...content, errorKey: err.key, errMeta: err.meta}]
    }, [])
    return errorResult;
}

export const generateRandom = (minMax, floating = true) => {
    return _.random(minMax.min, minMax.max, floating)
}

export const generateRandomPercent = (minMax, floating = true) => {
    return generateRandom(minMax, floating) / 100;
}
