import HttpStatus from 'http-status-codes';

export default class BusinessError extends Error {
    constructor(code = HttpStatus.INTERNAL_SERVER_ERROR, key, meta) {
        super(HttpStatus.getStatusText(code));
        this.code = code;
        this.key = key;
        this.meta = meta;
    }
}

export {default as HttpStatus} from 'http-status-codes';