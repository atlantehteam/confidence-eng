import argon2 from 'argon2';
import generator from 'generate-password';

export const hashPassword = (password) => {
  return argon2.hash(password);
}
  
export const checkPassword = (plainPass, hashedPass) => {
  return argon2.verify(hashedPass, plainPass);
}

export const generatePassword = (
  length = 7,
) =>
  generator.generate({length, numbers: true, lowercase: true, uppercase: false, strict: true})