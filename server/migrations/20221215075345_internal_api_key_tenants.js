
const defaultTenant = parseInt(process.env.RN_DEFAULT_TENANT);

export const up = async function(knex) {
    await knex.schema.table('internal_api_keys', (table) => {
        table.integer('tenantId').references('tenants.id').deferrable('deferred');
    })
    await knex('internal_api_keys').update({tenantId: defaultTenant});
    
    await knex.schema.alterTable('internal_api_keys', table => {
      table.integer('tenantId').notNullable().alter();
    });
};


export const down = async function(knex) {
    await knex.schema.table('internal_api_keys', (table) => {
        table.dropColumn('tenantId');
    })
};

export const config = { transaction: false }; // prevents cannot ALTER TABLE because it has pending trigger events