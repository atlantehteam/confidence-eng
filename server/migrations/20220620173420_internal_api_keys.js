
export const up = async function(knex) {
	await knex.schema.createTable('internal_api_keys', (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'));
        table.datetime('validUntil')
        table.uuid('apiKey').notNullable().unique()
        table.text('description')
        table.boolean('isActive').notNullable().defaultTo(true);
        table.datetime('createdAt').defaultTo(knex.fn.now())
        table.datetime('updatedAt').defaultTo(knex.fn.now())
    })
};

export const down = async function(knex) {
	return knex.schema
    .dropTableIfExists('internal_api_keys')
};
