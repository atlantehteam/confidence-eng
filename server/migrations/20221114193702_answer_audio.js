
export const up = async function(knex) {
    await knex.schema.table('golden_questions', (table) => {
        table.text('answerAudio');
    })
};


export const down = async function(knex) {
    await knex.schema.table('golden_questions', (table) => {
        table.dropColumn('answerAudio');
    })
};
