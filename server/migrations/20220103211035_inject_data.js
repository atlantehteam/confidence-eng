import _ from 'lodash';

// TODO: take from config when knex is solved https://github.com/knex/knex/issues/4983
const defaultTenant = parseInt(process.env.RN_DEFAULT_TENANT);
const injectionEnabled = process.env.RN_MIGRATION_NO_INJECTION !== 'true';

const testTenant = defaultTenant;
const password = '$argon2i$v=19$m=4096,t=3,p=1$kDKIcZmZFSWkYIo+95VWdg$bvFUsV6HLuz+a/8vK2xW6O0wLbnSIRkM69PO5PxaJqU'; // 123456

const userSuperAdmin = '11111111-1111-1111-1111-111111111111';

const levels = ['beginner', 'intermediate', 'advanced']
const levelsHeb = ['קל', 'בינוני', 'מתקדם']


const baseStudentId = '11111111-1111-1111-1111-111111111'; // should start with sufix 001
const baseChapterId = '22222222-1111-1111-1111-111111111'; // should start with sufix 001
const baseGoldenQuestionId = '33333333-1111-1111-1111-1111111'; // should start with sufix 00001
const baseMultiChoiceQuestionId = '44444444-1111-1111-1111-1111111'; // should start with sufix 00001

const permissionOptions = ['core', 'byTheWay', 'words', 'smallTalk']
const users = [
  {id: userSuperAdmin, tenantId: testTenant, name: 'Super Admin', email: '1@1.com', hashed_pass: password, role: 'superAdmin', selectedLevel: levels[0]},
  ..._.range(0, 20).map(index => ({
    id: `${baseStudentId}${String(index).padStart(3, '0')}`,
    tenantId: testTenant,
    name: `סטודנט בדיקה ${index}`,
    email: `student${index}@example.com`,
    hashed_pass: password,
    role: 'student',
    selectedLevel: levels.at(index % levels.length),
    permissions: _.sampleSize(permissionOptions, ((index + permissionOptions.length - 1) % permissionOptions.length) + 1),
  })),
]

const chapterContentDemo = `<h1 dir="ltr" style="text-align: center;"><strong>Content Of chapter {sufix} - level {level}</strong></h1>
<p dir="ltr" style="text-align: center;"><strong><iframe src="https://player.vimeo.com/video/696994215?h=a2930c7ed1&amp;title=0&amp;byline=0&amp;portrait=0&amp;badge=0" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe></strong></p>
<h1 style="direction: ltr; text-align: center;">This, That, These</h1>
<p style="text-align: center;"><strong><iframe src="https://player.vimeo.com/video/696994215?h=a2930c7ed1&amp;title=0&amp;byline=0&amp;portrait=0&amp;badge=0" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe></strong></p>`

export const injectDevelopment = async function(knex) {
  await knex('tenants').insert({id: testTenant, name: 'test tenants'});
  
  await knex('users').insert(users);

  const chapters = levels.flatMap((level, levelIndex) => _.range(1, 20).map(chapterSufix => ({
    id: `${baseChapterId}${levelIndex}${String(chapterSufix).padStart(2, '0')}`, tenantId: testTenant,
    name: `פרק ${chapterSufix} - שלב ${levelsHeb[levelIndex]}`, level, content: chapterContentDemo.replace('{sufix}', chapterSufix).replace('{level}', level),
  })))

  await knex('chapters').insert(chapters);

  const goldenQuestions = levels.flatMap((level, levelIndex) => _.range(1, 20).flatMap(chapterSufix => {
    const paddedChapterSufix = String(chapterSufix).padStart(2, '0');
    const chapterId = `${baseChapterId}${levelIndex}${paddedChapterSufix}`;
    return _.range(1, 5).map(questionIndex => ({
      id: `${baseGoldenQuestionId}${levelIndex}${paddedChapterSufix}${String(questionIndex).padStart(2, '0')}`,
      tenantId: testTenant, chapterId,
      title: `שאלת זהב ${questionIndex} בפרק ${chapterSufix} - שלב ${levelsHeb[levelIndex]}`,
      answer: `answer to question ${questionIndex} in chapter ${chapterSufix} - ${level} level`,
    }))
  }))

  await knex('golden_questions').insert(goldenQuestions);

  
  const multiChoiceQuestions = levels.flatMap((level, levelIndex) => _.range(1, 20).flatMap(chapterSufix => {
    const paddedChapterSufix = String(chapterSufix).padStart(2, '0');
    const chapterId = `${baseChapterId}${levelIndex}${paddedChapterSufix}`;
    return _.range(1, 5).map(questionIndex => {
        const questionId = `${baseMultiChoiceQuestionId}${levelIndex}${paddedChapterSufix}${String(questionIndex).padStart(2, '0')}`;
        return {
        id: questionId,
        tenantId: testTenant, chapterId,
        title: `שאלה אמריקאית ${questionIndex} בפרק ${chapterSufix} - שלב ${levelsHeb[levelIndex]}`,
        answers: [
          {title: `Answer 1 to question ${questionIndex}`, id: `${questionId}-1`, isCorrect: questionIndex % 3 === 0},
          {title: `Answer 2 to question ${questionIndex}`, id: `${questionId}-2`, isCorrect: questionIndex % 3 === 1},
          {title: `Answer 3 to question ${questionIndex}`, id: `${questionId}-3`, isCorrect: questionIndex % 3 === 2},
        ],
      }
    })
  }))

  await knex('mc_questions').insert(multiChoiceQuestions);


};

async function basicInjection(knex) {

}
export const up = async function(knex) {
  if (!injectionEnabled) {
    await knex('tenants').insert({id: defaultTenant, name: 'PROD tenant'});
  } else {
    await injectDevelopment(knex)
  }
  await basicInjection(knex);
};


export const down = async function(knex) {
  await knex.table('mc_questions').where({tenantId: testTenant}).delete();
  await knex.table('golden_questions').where({tenantId: testTenant}).delete();
  await knex.table('chapters').where({tenantId: testTenant}).delete();
  await knex.table('users').where({tenantId: testTenant}).delete();
  await knex.table('tenants').where({id: testTenant}).delete();
};
