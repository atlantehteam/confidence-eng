
export const up = async function(knex) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
    await knex.schema.createTable('tenants', (table) => {
        table.increments('id')
        table.text('name').notNullable().defaultTo('')
        table.datetime('createdAt').defaultTo(knex.fn.now())
        table.datetime('updatedAt').defaultTo(knex.fn.now())
    }).raw('ALTER SEQUENCE tenants_id_seq RESTART WITH 10000');

    /*users Table*/
    await knex.schema.createTable('users', (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'));
        table.integer('tenantId').notNullable().references('tenants.id').deferrable('deferred');
        table.text('name').notNullable()
        table.text('email').notNullable()
        table.text('hashed_pass')
        table.text('role').notNullable()
        table.text('selectedLevel').notNullable()
        table.specificType('permissions', 'text[]').notNullable().defaultTo('{}');
        table.datetime('createdAt').defaultTo(knex.fn.now())
        table.datetime('updatedAt').defaultTo(knex.fn.now())

        table.index(['tenantId', 'name'])
        table.index(['tenantId', 'email'])
        table.index(['tenantId', 'name', 'email'])
    })

    /*chapters Table*/
    await knex.schema.createTable('chapters', (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'));
        table.integer('tenantId').notNullable().references('tenants.id').deferrable('deferred');
        table.text('name').notNullable()
        table.text('level').notNullable()
        table.text('comments')
        table.text('content')
        table.text('vocabulary')
        table.text('summaryPDF')
        table.text('vocabularyPDF')
        table.specificType('downloads', 'jsonb[]').notNullable().defaultTo('{}');
        table.specificType('order','serial').notNullable()
        table.datetime('createdAt').defaultTo(knex.fn.now())
        table.datetime('updatedAt').defaultTo(knex.fn.now())

        table.index(['tenantId', 'name'])
        table.index(['tenantId', 'level'])
        table.index(['tenantId', 'level', 'name'])
    })

    /*golden_questions Table*/
    await knex.schema.createTable('golden_questions', (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'));
        table.integer('tenantId').notNullable().references('tenants.id').deferrable('deferred');
        table.uuid('chapterId').notNullable().references('chapters.id').deferrable('deferred');
        table.text('title').notNullable()
        table.text('answer').notNullable()
        table.specificType('order','serial').notNullable()
        table.datetime('createdAt').defaultTo(knex.fn.now())
        table.datetime('updatedAt').defaultTo(knex.fn.now())
    })

    /*mc_questions Table*/
    await knex.schema.createTable('mc_questions', (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'));
        table.integer('tenantId').notNullable().references('tenants.id').deferrable('deferred');
        table.uuid('chapterId').notNullable().references('chapters.id').deferrable('deferred');
        table.text('title').notNullable()
        table.specificType('answers', 'jsonb[]').notNullable()
        table.specificType('order','serial').notNullable()
        table.datetime('createdAt').defaultTo(knex.fn.now())
        table.datetime('updatedAt').defaultTo(knex.fn.now())
    })
}

export const down = function(knex) {
    return knex.schema
    .dropTableIfExists('mc_questions')
    .dropTableIfExists('golden_questions')
    .dropTableIfExists('chapters')
    .dropTableIfExists('users')
    .dropTableIfExists('tenants')
};
