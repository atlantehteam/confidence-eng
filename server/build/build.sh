COMMIT_ID=$(git rev-parse --short HEAD)
RANDOM=$(openssl rand -hex 4)
TAG="${COMMIT_ID}_${RANDOM}"
gcloud builds submit --config=build/cloudbuild.yaml --substitutions=_LOCATION="europe-west1",_REPOSITORY="main",_IMAGE="server",_TAG=${TAG} --project confidence-eng
