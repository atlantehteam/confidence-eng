import _ from 'lodash';
import config from '../config/config';
import nodemailer from 'nodemailer';
import BusinessError, { HttpStatus } from '../utils/BusinessError';

const options = {
    host: config.emails.smtp.host,
    port: config.emails.smtp.port,
    tls: {rejectUnauthorized: config.emails.smtp.rejectUnauthorized},
}
if (config.emails.smtp.user && config.emails.smtp.pass) {
    options.authMethod = config.emails.smtp.authMethod;
    options.auth = {
        user: config.emails.smtp.user,
        pass: config.emails.smtp.pass,
    }
}
const transporter  = nodemailer.createTransport(options);
transporter.verify()
.then(()=> console.log(`Mail client initialized successfully. Emails are ${config.emails.isEnabled ? 'ENABLED' : 'DISABLED'}`))
.catch((err) => {
    console.error(err);
    console.error('Mail client FAILED to initialize. Emails will not be sent')
})

export const sendMail = async function({to, cc, bcc, subject, html, attachments, throwOnFail}) {
    try {
        const {isEnabled} = config.emails;
        const uniqueTo = _.uniq(to)
        const uniqueCC = _.uniq(cc)
        const uniqueBCC = _.uniq(bcc)
        if (!isEnabled) {
            console.log(`Emails are disabled. Will not send email to [${uniqueTo}, ${uniqueCC}, ${uniqueBCC}], with subject '${subject}'`)
            return;
        }

        console.log(`Sending email\nto [${uniqueTo}], cc [${uniqueCC}], bcc [${uniqueBCC}] with subject '${subject}'`)
        await transporter.sendMail({to: uniqueTo, cc: uniqueCC, bcc: uniqueBCC, from: config.emails.fromEmail, subject, html, attachments})
    } catch (err) {
        console.error(err);
        console.log(`Failed to send email to [${to}], cc [${cc}], bcc [${bcc}], with subject '${subject}'`)
        if (throwOnFail) {
            throw new BusinessError(HttpStatus.INTERNAL_SERVER_ERROR, 'mailSendFailed');
        }
    }
};
