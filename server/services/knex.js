import config from '../config/config';
import createKnex from 'knex';

// fix count & avg return as string
import pg from 'pg'
pg.types.setTypeParser(pg.types.builtins.INT8, parseInt) 
pg.types.setTypeParser(pg.types.builtins.NUMERIC, parseFloat)

const knex = createKnex({
    client: 'pg',
    asyncStackTraces: true,
    connection: {
        host : config.knex.host,
        user : config.knex.user,
        password : config.knex.password,
        database : config.knex.database,
        port: config.knex.port,
        timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
        ssl: config.knex.ssl,
    },
    pool: {
        min: 0,
        max: 3,
    },
});

export default knex;
