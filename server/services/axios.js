import _ from 'lodash';

export const formatAxiosError = err => {
    if (!err.isAxiosError) {
        return err;
    }
    const newErr = new Error(err.message);
    newErr.responseData = _.get(err, 'response.data');
    newErr.responseStatus = _.get(err, 'response.status');
    if (!newErr.responseStatus) {
        newErr.axiosError = err.toJSON();
    }
    return newErr;
}