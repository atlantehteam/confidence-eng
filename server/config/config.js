
export default {
    host: {
        service: process.env.RN_SERVICE_HOST,
        app: process.env.RN_APP_HOST,
    },
    service: {
        host: process.env.RN_HOST,
        defaultTenant: parseInt(process.env.RN_DEFAULT_TENANT),
    },
    migration: {
        injectEnabled: process.env.RN_MIGRATION_NO_INJECTION !== 'true',
    },
    knex: {
        host : process.env.KNEX_HOST,
        user : process.env.KNEX_USER,
        password : process.env.KNEX_PASSWORD,
        database : process.env.KNEX_DB,
        port : process.env.KNEX_PORT,
        ssl: process.env.KNEX_SSL === 'true',
    },
    accessTokenSecret: process.env.ACCESS_TOKEN_SECRET,
    passwordSaltRounds: 10,
    request: {
        sizeLimit: '1mb',
    },
    emails: {
        isEnabled: process.env.RN_SMTP_ENABLED === 'true',
        fromEmail: process.env.RN_SMTP_FROM_EMAIL || 'no-reply@confidence.me',
        smtp: {
            host: process.env.RN_SMTP_HOST,
            port: parseInt(process.env.RN_SMTP_PORT) || 25,
            authMethod : process.env.RN_SMTP_AUTH_METHOD || 'LOGIN',
            user: process.env.RN_SMTP_USER,
            pass: process.env.RN_SMTP_PASS,
            rejectUnauthorized: process.env.RN_SMTP_REJECT_UNAUTHORIZED !== 'false',
        },
    },
    gcs: {
        privateBucket: process.env.RN_GCS_PRIVATE_BUCKET || 'unkown-bucket',
        defaultDurationSeconds: 3 * 60 * 60,
    },

}