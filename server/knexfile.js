// Update with your config settings.

export default {
  default: {
    client: 'pg',
    connection: {
      host : process.env.KNEX_HOST,
      user : process.env.KNEX_USER,
      password : process.env.KNEX_PASSWORD,
      database : process.env.KNEX_DB,
      port : process.env.KNEX_PORT,
      ssl: process.env.KNEX_SSL === 'true',
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
    },
  },
};
