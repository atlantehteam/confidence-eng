import express from 'express';
import {registerRoutes} from './routes';
import config from './config/config';
import cors from 'cors'
import cookieParser from 'cookie-parser';

const app = express();

app.disable('x-powered-by');
app.use(cors({origin: [/http:\/\/localhost:*/], maxAge: 600}));
app.use(cookieParser());
app.use(express.json({limit: config.request.sizeLimit}))
app.use(express.urlencoded({extended: true}))

registerRoutes(app);

const listener = app.listen(process.env.PORT || 5000, () => console.log(`Listening on port`, listener.address().port))