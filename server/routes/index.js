import express from 'express'
import errorsRoute from './errorsRoute'
import loginRoute from './controllers/login'
import chaptersRoute from './controllers/chapters'
import goldenQuestionsRoute from './controllers/goldenQuestions'
import mcQuestionsRoute from './controllers/mcQuestions'
import usersRoute from './controllers/users'
import authRoute from './controllers/auth'
import btwRoute from './controllers/btw'
import wordsRoute from './controllers/words'
import smallTalkRoute from './controllers/smallTalk'
import internalRoute from './controllers/internal'
import validateTokenMiddleware, { validateInternalApiKey } from '../utils/validateTokenMiddleware'
import helmet from 'helmet'

function getUserAutorizedRouter() {
    const authorizedRouter = express.Router({mergeParams: true});
    authorizedRouter.use(validateTokenMiddleware);

    chaptersRoute(authorizedRouter);
    goldenQuestionsRoute(authorizedRouter);
    mcQuestionsRoute(authorizedRouter);
    usersRoute(authorizedRouter);
    authRoute(authorizedRouter);
    btwRoute(authorizedRouter);
    wordsRoute(authorizedRouter);
    smallTalkRoute(authorizedRouter);

    return authorizedRouter;
}

function getApiRouter() {
    const apiRouter = express.Router({mergeParams: true});
    apiRouter.use(helmet());

    apiRouter.get('/ping', (req, res) => res.send(new Date()));
    
    loginRoute(apiRouter);
    routesAuthenticator(apiRouter, validateInternalApiKey, [internalRoute])

    const authorizedRouter = getUserAutorizedRouter()
    apiRouter.use(authorizedRouter);

    return apiRouter;
}

function routesAuthenticator(app, authMiddleware, routes) {
    const authenticatedRouter = express.Router({mergeParams: true});
    
    // proxy router and inject authMiddleware to all methods
    // Done to prevent app.use(authMiddleware) as it blocks rest of api route
    const handlers = {get: function(target, prop) {
        return function() {
            const [method, ...rest] = arguments;
            target[prop](method, authMiddleware, ...rest);
        }
    }}
    const proxy = new Proxy(authenticatedRouter, handlers)

    routes.forEach(route => route(proxy));

    app.use(authenticatedRouter);

    return authenticatedRouter;
}

export const registerRoutes = (app) => {    
    const apiRouter = getApiRouter();
    
    app.use('/api', apiRouter)

    errorsRoute(app); // should always be last
};
