import _  from 'lodash'
import HttpStatus from 'http-status-codes'
import BusinessError from '../../utils/BusinessError'
import knex from '../../services/knex'
import {mapToDBFields, addWhereClause, mapToDBKey} from '../../utils/dbUtils'

function getJoins(query) {
    return query
        // .innerJoin({p: 'publishers'}, 'ch.publisherId', '=', 'p.id')
        // .innerJoin({co: 'contexts'}, 'ch.contextId', '=', 'co.id')
}
function getSelect(query) {
    return query
    .select(
        'u.*',
        // {
        //     goldenQuestionsCount: knex('golden_questions').where({chapterId: knex.raw('ch.id')}).count(),
        //     publisher: knex.raw(`json_build_object(
        //         'id', p.id,
        //         'name', p.name,
        //         'slug', p.slug
        //     )`),
        //     context: knex.raw(`json_build_object(
        //         'id', co.id,
        //         'name', co.name
        //     )`),
        // },
    );
}

export const getUsers = async function({where, rehydrate = true, offset = 0, limit = -1, count = true, orderBy, order}) {
    
    orderBy = orderBy || 'user.updatedAt';
    order = order || 'desc';

    const dbMap = {user: 'u'}
    const mappedWhere = mapToDBFields(where, dbMap);

    let query = knex({u: 'users'})
    query = addWhereClause(query, mappedWhere);
    query = getJoins(query);


    const mappedOrderBy = mapToDBKey(orderBy, dbMap);

    let recordsQuery = getSelect(query.clone())
        .offset(offset)
        .orderBy(mappedOrderBy, order)
    
    const intLimit = parseInt(limit);
    if (intLimit >= 0) {
        recordsQuery = recordsQuery.limit(intLimit)
    }
    const promises = [recordsQuery]

    if (count) {
        promises.push(query.clone().countDistinct('u.id'))
    }
       
    const [users, total] = await Promise.all(promises)
    
    let toReturn = users;
    // ugly hack to prevent filters from removing aggregates from the entity :/
    // if (rehydrate && users.length) {
    //     const userIds = users.map(l => l.id);
    //     const where = {user: {id: {op: 'in', value: userIds}}};
    //     const rehydrated = await getUsers({where, rehydrate: false, noCount: true, includeDeleted, limit: -1, orderBy, order})
    //     toReturn = rehydrated.users;
    // }

    let _meta = undefined;
    if (total) {_meta = {total: total[0].count}}

    return {_meta, users: toReturn};
}

export const getUserById = async function(id, options = {}) {
    const where = {$and: [
        {user: {id}},
        {...options.where},
    ]}
    const {users: [item]} = await getUsers({where, limit: 1, noCount: true})
    if (!item) {
        throw new BusinessError(HttpStatus.NOT_FOUND);
    }
    return item;
}

const validUserFields = ['tenantId', 'name', 'email', 'hashed_pass', 'role', 'selectedLevel', 'permissions'];
export const addUser = async function(data) {
    const validUser = _.pick(data, validUserFields);
    const trxResult = await knex.transaction(async trx => {
        const [existingItem] = await trx('users').where({email: data.email}).limit(1).select('id')
        if (existingItem) {
            throw new BusinessError(HttpStatus.CONFLICT, 'emailExists');
        }
        let [item] = await trx('users').insert(validUser).returning('*');

        return item;
    })

    const result = await getUserById(trxResult.id)
    return result;
}


export const updateUser = async function(id, data) {
    const validUser = _.pick(data, validUserFields);
    const updatedItem = await knex.transaction(async trx => {
        if (data.email) {
            const [existingPublisher] = await trx('users')
                .where(_.pick(data, ['email']))
                .whereNot({id})
                .limit(1).select('id')
            if (existingPublisher) {
                throw new BusinessError(HttpStatus.CONFLICT, 'emailExists');
            }
        }
        const [item] = await trx('users').update({...validUser, updatedAt: new Date()}).where({id}).returning('*');

        return item;
    })
    const result = await getUserById(updatedItem.id)
    return result
}

export const deleteUser = async function(id) {
    const [result] = await knex('users').where({id}).del().returning('*');
    return result;
}
