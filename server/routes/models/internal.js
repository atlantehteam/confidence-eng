import knex from '../../services/knex'

export const getValidInternalApiKey = async (apiKey) => {
    const validApiKey = await knex('internal_api_keys')
    .where({apiKey, isActive: true})
    .where(function() {
        this.whereNull('validUntil')
        .orWhere('validUntil', ">=", knex.fn.now())
    })
    .first();
    return validApiKey;
}
