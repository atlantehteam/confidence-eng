import _  from 'lodash'
import HttpStatus from 'http-status-codes'
import BusinessError from '../../utils/BusinessError'
import knex from '../../services/knex'
import {mapToDBFields, addWhereClause, mapToDBKey} from '../../utils/dbUtils'

function getMCQuestionJoins(query) {
    return query
}
function getMCQuestionSelect(query) {
    return query
    .select(
        'mcq.*',

    );
}

export const getMCQuestions = async function({where, rehydrate = true, offset = 0, limit = -1, count, orderBy, order}) {
    
    orderBy = orderBy || 'mcQuestion.order';
    order = order || 'asc';

    const dbMap = {mcQuestion: 'mcq'}
    const mappedWhere = mapToDBFields(where, dbMap);

    let query = knex({mcq: 'mc_questions'})
    query = addWhereClause(query, mappedWhere);
    query = getMCQuestionJoins(query);


    const mappedOrderBy = mapToDBKey(orderBy, dbMap);

    let mcQuestionsQuery = getMCQuestionSelect(query.clone())
        .offset(offset)
        .orderBy(mappedOrderBy, order)
    
    const intLimit = parseInt(limit);
    if (intLimit >= 0) {
        mcQuestionsQuery = mcQuestionsQuery.limit(intLimit)
    }
    const promises = [mcQuestionsQuery]

    if (count) {
        promises.push(query.clone().countDistinct('ch.id'))
    }
       
    const [mcQuestions, total] = await Promise.all(promises)
    
    let mcQuestionsToReturn = mcQuestions;
    // ugly hack to prevent filters from removing aggregates from the entity :/
    // if (rehydrate && mcQuestions.length) {
    //     const mcQuestionIds = mcQuestions.map(l => l.id);
    //     const where = {mcQuestion: {id: {op: 'in', value: mcQuestionIds}}};
    //     const rehydrated = await getMCQuestions({where, rehydrate: false, noCount: true, includeDeleted, limit: -1, orderBy, order})
    //     mcQuestionsToReturn = rehydrated.mcQuestions;
    // }

    let _meta = undefined;
    if (total) {_meta = {total: total[0].count}}

    return {_meta, mcQuestions: mcQuestionsToReturn};
}

export const getMCQuestionById = async function(id, options = {}) {
    const where = {$and: [
        {mcQuestion: {id}},
        {...options.where},
    ]}
    const {mcQuestions: [item]} = await getMCQuestions({where, limit: 1, noCount: true})
    if (!item) {
        throw new BusinessError(HttpStatus.NOT_FOUND);
    }
    return item;
}

export const addMCQuestion = async function(data) {
    let [item] = await knex('mc_questions').insert(data).returning('*');

    const result = await getMCQuestionById(item.id)
    return result;
}


export const updateMCQuestion = async function(id, data) {
    const [item] = await knex('mc_questions').update({...data, updatedAt: new Date()}).where({id}).returning('*');

    const result = await getMCQuestionById(item.id)
    return result

}

export const updateBulkMCQuestions = async function (bulkData) {
    const now = new Date()
    await knex.transaction(async trx => {
        const promises = bulkData.map(bd => trx('mc_questions').where({id: bd.id}).update({...bd.data, updatedAt: now}))
        await Promise.all(promises);
    });

    const where = {mcQuestion: {id: {op: 'in', value: bulkData.map(b => b.id)}}};
    const {mcQuestions} = await getMCQuestions({where})
    return mcQuestions;
}
export const deleteMCQuestion = async function(id) {
    const [result] = await knex('mc_questions').where({id}).del().returning('*');
    return result;
}
