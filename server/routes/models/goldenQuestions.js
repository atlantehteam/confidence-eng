import _  from 'lodash'
import HttpStatus from 'http-status-codes'
import BusinessError from '../../utils/BusinessError'
import knex from '../../services/knex'
import {mapToDBFields, addWhereClause, mapToDBKey} from '../../utils/dbUtils'
import {v4} from 'uuid';
import { uploadMediaOrResolve } from '../../utils/imageUtils'
import config from '../../config/config';

function getGoldenQuestionJoins(query) {
    return query
        // .innerJoin({p: 'publishers'}, 'ch.publisherId', '=', 'p.id')
        // .innerJoin({co: 'contexts'}, 'ch.contextId', '=', 'co.id')
}

function getGoldenQuestionSelect(query) {
    return query
    .select(
        'gq.*',
        // {
        //     goldenQuestionsCount: knex('golden_questions').where({goldenQuestionId: knex.raw('ch.id')}).count(),
        //     publisher: knex.raw(`json_build_object(
        //         'id', p.id,
        //         'name', p.name,
        //         'slug', p.slug
        //     )`),
        //     context: knex.raw(`json_build_object(
        //         'id', co.id,
        //         'name', co.name
        //     )`),
        // },
    );
}

async function uploadGoldenQuestionMedia(id, chapterId, data) {
    const mediaKeys = ['answerAudio'];
    const mediaPromises = mediaKeys.map(key => 
        uploadMediaOrResolve(config.gcs.privateBucket, `chapters/${chapterId}/static/answers/${id}` ,data[key], key, {format: 'mp3', output: 'gsPath'}),
    )
    const mediaResults = await Promise.all(mediaPromises);
    const mediaFiles = _.assign({}, ...mediaResults);
    return mediaFiles;
}

export const getGoldenQuestions = async function({where, rehydrate = true, offset = 0, limit = -1, count, orderBy, order}) {
    
    orderBy = orderBy || 'goldenQuestion.order';
    order = order || 'asc';

    const dbMap = {goldenQuestion: 'gq'}
    const mappedWhere = mapToDBFields(where, dbMap);

    let query = knex({gq: 'golden_questions'})
    query = addWhereClause(query, mappedWhere);
    query = getGoldenQuestionJoins(query);


    const mappedOrderBy = mapToDBKey(orderBy, dbMap);

    let goldenQuestionsQuery = getGoldenQuestionSelect(query.clone())
        .offset(offset)
        .orderBy(mappedOrderBy, order)
    
    const intLimit = parseInt(limit);
    if (intLimit >= 0) {
        goldenQuestionsQuery = goldenQuestionsQuery.limit(intLimit)
    }
    const promises = [goldenQuestionsQuery]

    if (count) {
        promises.push(query.clone().countDistinct('ch.id'))
    }
       
    const [goldenQuestions, total] = await Promise.all(promises)
    
    let goldenQuestionsToReturn = goldenQuestions;
    // ugly hack to prevent filters from removing aggregates from the entity :/
    // if (rehydrate && goldenQuestions.length) {
    //     const goldenQuestionIds = goldenQuestions.map(l => l.id);
    //     const where = {goldenQuestion: {id: {op: 'in', value: goldenQuestionIds}}};
    //     const rehydrated = await getGoldenQuestions({where, rehydrate: false, noCount: true, includeDeleted, limit: -1, orderBy, order})
    //     goldenQuestionsToReturn = rehydrated.goldenQuestions;
    // }

    let _meta = undefined;
    if (total) {_meta = {total: total[0].count}}

    return {_meta, goldenQuestions: goldenQuestionsToReturn};
}

export const getGoldenQuestionById = async function(id, options = {}) {
    const where = {$and: [
        {goldenQuestion: {id}},
        {...options.where},
    ]}
    const {goldenQuestions: [item]} = await getGoldenQuestions({where, limit: 1, noCount: true})
    if (!item) {
        throw new BusinessError(HttpStatus.NOT_FOUND);
    }
    return item;
}

export const addGoldenQuestion = async function(chapterId, data) {
    const questionId = v4();
    const mediaFiles = await uploadGoldenQuestionMedia(questionId, chapterId, data);
    
    let [item] = await knex('golden_questions').insert({...data, ...mediaFiles, chapterId, id: questionId}).returning('*');

    const result = await getGoldenQuestionById(item.id)
    return result;
}


export const updateGoldenQuestion = async function(id, chapterId, data) {
    const mediaFiles = await uploadGoldenQuestionMedia(id, chapterId, data);

    const [item] = await knex('golden_questions').update({...data, ...mediaFiles, updatedAt: new Date()}).where({id}).returning('*');

    const result = await getGoldenQuestionById(item.id)
    return result

}

export const updateBulkGoldenQuestions = async function (bulkData) {
    const now = new Date()
    await knex.transaction(async trx => {
        const promises = bulkData.map(bd => trx('golden_questions').where({id: bd.id}).update({...bd.data, updatedAt: now}))
        await Promise.all(promises);
    });

    const where = {goldenQuestion: {id: {op: 'in', value: bulkData.map(b => b.id)}}};
    const {goldenQuestions} = await getGoldenQuestions({where})
    return goldenQuestions;
}
export const deleteGoldenQuestion = async function(id) {
    const [result] = await knex('golden_questions').where({id}).del().returning('*');
    return result;
}
