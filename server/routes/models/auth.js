import _ from 'lodash'
import knex from '../../services/knex'

export const getUserRoles = async (jwtUser) => {
    if (jwtUser.role === 'superAdmin') {return [];}
    const roles = await knex('roles').where({userId: jwtUser.id});
    const groupedRoles = _.groupBy(roles, r => r.entityName);
    return groupedRoles;
}