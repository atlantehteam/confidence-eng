import _  from 'lodash'
import HttpStatus from 'http-status-codes'
import config from '../../config/config';
import BusinessError from '../../utils/BusinessError'
import knex from '../../services/knex'
import {mapToDBFields, addWhereClause, mapToDBKey} from '../../utils/dbUtils'
import { uploadMediaOrResolve } from '../../utils/imageUtils'
import {v4} from 'uuid';

function getChapterJoins(query) {
    return query
        // .innerJoin({p: 'publishers'}, 'ch.publisherId', '=', 'p.id')
        // .innerJoin({co: 'contexts'}, 'ch.contextId', '=', 'co.id')
}

function getChapterSelect(query, includes = '*') {
    const allIncludes = includes.split(',').concat(['id', 'tenantId'])
    const includesToUse = allIncludes.includes('*') ? ['*'] : _.uniq(allIncludes);
    return query
    .select(
        ...includesToUse.filter(_.identity).map(i => `ch.${i}`),
        {
            goldenQuestionsCount: knex('golden_questions').where({chapterId: knex.raw('ch.id')}).count(),
            mcQuestionsCount: knex('mc_questions').where({chapterId: knex.raw('ch.id')}).count(),
        //     publisher: knex.raw(`json_build_object(
        //         'id', p.id,
        //         'name', p.name,
        //         'slug', p.slug
        //     )`),
        //     context: knex.raw(`json_build_object(
        //         'id', co.id,
        //         'name', co.name
        //     )`),
        },
    );
}

export const getChapters = async function({where, rehydrate = true, offset = 0, limit = -1, count, orderBy, order, includes}) {
    
    orderBy = orderBy || 'chapter.order';
    order = order || 'asc';

    const dbMap = {chapter: 'ch'}
    const mappedWhere = mapToDBFields(where, dbMap);

    let query = knex({ch: 'chapters'})
    query = addWhereClause(query, mappedWhere);
    query = getChapterJoins(query);


    const mappedOrderBy = mapToDBKey(orderBy, dbMap);

    let chaptersQuery = getChapterSelect(query.clone(), includes)
        .offset(offset)
        .orderBy(mappedOrderBy, order)
    
    const intLimit = parseInt(limit);
    if (intLimit >= 0) {
        chaptersQuery = chaptersQuery.limit(intLimit)
    }
    const promises = [chaptersQuery]

    if (count) {
        promises.push(query.clone().countDistinct('ch.id'))
    }
       
    const [chapters, total] = await Promise.all(promises)
    
    let chaptersToReturn = chapters;
    // ugly hack to prevent filters from removing aggregates from the entity :/
    // if (rehydrate && chapters.length) {
    //     const chapterIds = chapters.map(l => l.id);
    //     const where = {chapter: {id: {op: 'in', value: chapterIds}}};
    //     const rehydrated = await getChapters({where, rehydrate: false, noCount: true, includeDeleted, limit: -1, orderBy, order})
    //     chaptersToReturn = rehydrated.chapters;
    // }

    let _meta = undefined;
    if (total) {_meta = {total: total[0].count}}

    return {_meta, chapters: chaptersToReturn};
}

export const getChapterById = async function(id, options = {}) {
    const where = {$and: [
        {chapter: {id}},
        {...options.where},
    ]}
    const {chapters: [item]} = await getChapters({where, limit: 1, noCount: true})
    if (!item) {
        throw new BusinessError(HttpStatus.NOT_FOUND);
    }
    return item;
}

async function uploadDownloadables(chapterId, downloads) {
    if (!downloads) {return {}}

    const mediaPromises = downloads.map(async dn => {
        const key = `download-${dn.id}`;
        const uploaded = await uploadMediaOrResolve(config.gcs.privateBucket, `chapters/${chapterId}/static` ,dn.content, key, {format: 'pdf', output: 'gsPath'})
        return {...dn, content: uploaded[key]}
    })
    const allUploads = await Promise.all(mediaPromises);
    return {downloads: allUploads};
}

async function uploadChapterMedia(id, data) {
    const mediaKeys = ['vocabularyPDF', 'summaryPDF'];
    const mediaPromises = mediaKeys.map(key => uploadMediaOrResolve(config.gcs.privateBucket, `chapters/${id}/static` ,data[key], key, {format: 'pdf', output: 'gsPath'}))
    mediaPromises.push(uploadDownloadables(id, data.downloads))
    const mediaResults = await Promise.all(mediaPromises);
    const mediaFiles = _.assign({}, ...mediaResults);
    return mediaFiles;
}

export const addChapter = async function(data) {
    const chapterId = v4();
    const mediaFiles = await uploadChapterMedia(chapterId, data);
    const [item] = await knex('chapters').insert({...data, ...mediaFiles, id: chapterId}).returning('*');

    const result = await getChapterById(item.id)
    return result;
}


export const updateChapter = async function(id, data) {
    const mediaFiles = await uploadChapterMedia(id, data);
    const [item] = await knex('chapters').update({...data, ...mediaFiles, updatedAt: new Date()}).where({id}).returning('*');

    const result = await getChapterById(item.id)
    return result

}

export const updateBulkChapters = async function (bulkData) {
    const now = new Date()
    await knex.transaction(async trx => {
        const promises = bulkData.map(bd => trx('chapters').where({id: bd.id}).update({...bd.data, updatedAt: now}))
        await Promise.all(promises);
    });

    const where = {chapter: {id: {op: 'in', value: bulkData.map(b => b.id)}}};
    const {chapters} = await getChapters({where})
    return chapters;
}

export const deleteChapter = async function(id) {
    const result = await knex.transaction(async trx => {
        const [[item]] = await Promise.all([
            trx('chapters').where({id}).del().returning('*'),
            trx('golden_questions').where({chapterId: id}).del().returning('*'),
            trx('mc_questions').where({chapterId: id}).del().returning('*'),
        ])
        return item;
    })
    return result;
}
