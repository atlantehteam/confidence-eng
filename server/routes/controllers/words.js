import _ from 'lodash';
import BusinessError, { HttpStatus } from '../../utils/BusinessError';
import {routeWrapper} from '../../utils/generalUtils'
import {authorize} from '../../utils/rba'
import {signUrls} from '../../utils/fileUtils';

const wordsPages = {
    list: {
        content: [
            {type: 'link:pdf', title: 'לצפייה במילים', url: 'gs://confidence-eng-shared/modules/words/pdf/5000_words.pdf', sign: true, sx: {mt: 4}},
        ],
    },
}

async function getPage(req, res) {
    const {wordsPage} = req.params;

    const page = wordsPages[wordsPage];
    if (!page) {
        throw new BusinessError(HttpStatus.NOT_FOUND, 'pageNotFound');
    }
    const filteredPage = authorize.read('words', 'words', req).filter(page)
    filteredPage.content = await signUrls(filteredPage.content)
    res.send(filteredPage);
}

export default (app) => {
    app.get('/pages/words/:wordsPage', routeWrapper(getPage));
};
