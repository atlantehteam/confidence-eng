import HttpStatus from 'http-status-codes'
import knex from '../../services/knex'
import BusinessError from '../../utils/BusinessError'
import {routeWrapper} from '../../utils/generalUtils'
import { authorize, enforceAuthorization } from '../../utils/rba'
import * as userModel from '../models/users'

async function getUserSettings(userId) {
    const [user] = await knex('users')
        .where({id: userId})
        .select(['id', 'tenantId', 'name', {primaryRole: 'role'}, 'email', 'selectedLevel', 'permissions']).limit(1);
    if (!user) {
        throw new BusinessError(HttpStatus.UNAUTHORIZED, 'unknownUser');
    }
    return user;
}
const getCurrentUser = async (req, res) => {
    const userSettings = await getUserSettings(req.user.id)
    
    const filteredUser = authorize.read('userSettings', userSettings, req).filter(userSettings);
    return res.send(filteredUser);
}

const updateUserSettings = async (req, res) => {
    const userId = req.user.id
    const user = await userModel.getUserById(userId);

    const validFields = authorize.update('userSettings', user, req).filter(req.body);
    await userModel.updateUser(userId, validFields)

    const userSettings = await getUserSettings(req.user.id)
    
    const filteredUser = authorize.read('userSettings', userSettings, req).filter(userSettings);
    return res.send(filteredUser);
};

export default (app) => {
    app.get('/auth/user', routeWrapper(getCurrentUser));
    app.patch('/auth/user/settings', routeWrapper(updateUserSettings));
};
