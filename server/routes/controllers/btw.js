import _ from 'lodash';
import BusinessError, { HttpStatus } from '../../utils/BusinessError';
import {routeWrapper} from '../../utils/generalUtils'
import {authorize} from '../../utils/rba'
import {signUrls} from '../../utils/fileUtils';

const btwPages = {
    welcome: {
        content: [
            {type: 'iframe', url: 'https://player.vimeo.com/video/532616555?title=0&byline=0&portrait=0'},
        ],
    },
    intro: {
        content: [
            {type: 'audio', url: 'gs://confidence-eng-shared/modules/byTheWay/audio/Part1Introduction-fix.mp3', sign: true},
            {type: 'link:pdf', title: 'Introduction', url: 'gs://confidence-eng-shared/modules/byTheWay/pdf/1e.pdf', sign: true, sx: {mt: 4}},
            {type: 'link:pdf', title: 'הקדמה', url: 'gs://confidence-eng-shared/modules/byTheWay/pdf/1e-heb.pdf', sign: true},
            {type: 'link:pdf', title: 'כלים ומקורות', url: 'gs://confidence-eng-shared/modules/byTheWay/pdf/1e-tools-heb.pdf', sign: true},
        ],
    },
    vocabulary: {
        content: [
            {type: 'audio', url: 'gs://confidence-eng-shared/modules/byTheWay/audio/Part-2-Vocabulary-fix-1.mp3', sign: true},
            {type: 'link:pdf', title: 'Increase your treasure', url: 'gs://confidence-eng-shared/modules/byTheWay/pdf/2e.pdf', sign: true, sx: {mt: 4}},
            {type: 'link:pdf', title: 'הגדל את האוצר שלך', url: 'gs://confidence-eng-shared/modules/byTheWay/pdf/2e-heb.pdf', sign: true},
        ],
    },
    how: {
        content: [
            {type: 'audio', url: 'gs://confidence-eng-shared/modules/byTheWay/audio/Part-3-Audio-Video.mp3', sign: true},
            {type: 'link:pdf', title: 'English by the way', url: 'gs://confidence-eng-shared/modules/byTheWay/pdf/3e.pdf', sign: true, sx: {mt: 4}},
            {type: 'link:pdf', title: 'אנגלית על הדרך', url: 'gs://confidence-eng-shared/modules/byTheWay/pdf/3e-heb.pdf', sign: true},
        ],
    },
    method: {
        content: [
            {type: 'audio', url: 'gs://confidence-eng-shared/modules/byTheWay/audio/Part-4-Deeper-Into-the-Method.mp3', sign: true},
            {type: 'link:pdf', title: 'Entering the vault', url: 'gs://confidence-eng-shared/modules/byTheWay/pdf/4e.pdf', sign: true, sx: {mt: 4}},
            {type: 'link:pdf', title: 'נכנסים לכספת', url: 'gs://confidence-eng-shared/modules/byTheWay/pdf/4e-heb.pdf', sign: true},
        ],
    },
    advanced: {
        content: [
            {type: 'audio', url: 'gs://confidence-eng-shared/modules/byTheWay/audio/Part-5-Reading-Material.mp3', sign: true},
            {type: 'link:pdf', title: 'Advanced tools', url: 'gs://confidence-eng-shared/modules/byTheWay/pdf/5e.pdf', sign: true, sx: {mt: 4}},
            {type: 'link:pdf', title: 'כלים מתקדמים', url: 'gs://confidence-eng-shared/modules/byTheWay/pdf/5e-heb.pdf', sign: true},
        ],
    },
    summary: {
        content: [
            {type: 'audio', url: 'gs://confidence-eng-shared/modules/byTheWay/audio/Part-6-Summary-and-Mindset.mp3', sign: true},
            {type: 'link:pdf', title: 'What\'s next?', url: 'gs://confidence-eng-shared/modules/byTheWay/pdf/6e.pdf', sign: true, sx: {mt: 4}},
            {type: 'link:pdf', title: '?מה הלאה', url: 'gs://confidence-eng-shared/modules/byTheWay/pdf/6e-heb.pdf', sign: true},
        ],
    },
}

async function getBTWPage(req, res) {
    const {btwPage} = req.params;

    const page = btwPages[btwPage];
    if (!page) {
        throw new BusinessError(HttpStatus.NOT_FOUND, 'pageNotFound');
    }
    const filteredPage = authorize.read('btw', 'byTheWay', req).filter(page)
    filteredPage.content = await signUrls(filteredPage.content)
    res.send(filteredPage);
}

export default (app) => {
    app.get('/pages/btw/:btwPage', routeWrapper(getBTWPage));
};
