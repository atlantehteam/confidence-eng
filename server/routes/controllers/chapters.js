import _ from 'lodash';
import BusinessError, { HttpStatus } from '../../utils/BusinessError';
import {routeWrapper} from '../../utils/generalUtils'
import {authorize, enforceAuthorization} from '../../utils/rba'
import {signFileUrl} from '../../utils/fileUtils';
import * as chapterModel from '../models/chapters'

function validateChapter(oldChapter, newChapter) {
    return newChapter;
}

async function prepareChapterForClient(chapter) {
    const updatedChapter = {...chapter};

    const propsToSign = ['summaryPDF', 'vocabularyPDF'];
    const promises = propsToSign.map(async prop => {
        if (!chapter[prop]) {return}
        const signedUrl = await signFileUrl(chapter[prop])
        updatedChapter[prop] = signedUrl;
    })

    const downloadsPromises = (chapter.downloads || []).map(async download => {
        return {...download, content: download.content && await signFileUrl(download.content)}
    })
    const signedDownloads = await Promise.all(downloadsPromises)
    updatedChapter.downloads = signedDownloads
    
    await Promise.all(promises)
    
    return updatedChapter;
}

const addChapter = async (req, res) => {
    const {tenantId} = req.user;
    const {level} = req.params;

    const validFields = req.body;

    const createPolicy = authorize.create('chapter', {...validFields, tenantId}, req);
    const chapterToSave = createPolicy.filter(validFields);

    const validatedChapter = validateChapter(null, {...chapterToSave, tenantId, level});
    const dbChapter = await chapterModel.addChapter(validatedChapter)
    
    const readPolicy = authorize.read('chapter', dbChapter, req);
    const filteredChapter = readPolicy.filter(dbChapter)
    const resultChapter = await prepareChapterForClient(filteredChapter);

    res.send(resultChapter);
};

const updateChapter = async (req, res) => {
    const {id} = req.params;
    
    const validFields = req.body;

    const where = enforceAuthorization('chapter', req)
    const chapter = await chapterModel.getChapterById(id, {where});
    const updatePolicy = authorize.update('chapter', chapter, req);
    
    const validChapter = updatePolicy.filter(validFields)
    const validatedChapter = validateChapter(chapter, validChapter);

    let updatedChapter = await chapterModel.updateChapter(id, validatedChapter)

    const readPolicy = authorize.read('chapter', updatedChapter, req);
    const filteredChapter = readPolicy.filter(updatedChapter);
    const resultChapter = await prepareChapterForClient(filteredChapter);

    res.send(resultChapter);
};

const reorderChapters = async (req, res) => {
    
    const chapterIds = req.body;

    const where = {$and: [
        enforceAuthorization('chapter', req),
        {chapter: {id: {op: 'in', value: chapterIds}}},
    ]}
    const {chapters} = await chapterModel.getChapters({where});
    const chapterMap = _.keyBy(chapters, 'id')
    const missingChapters = chapterIds.filter(chId => !chapterMap[chId])
    if (missingChapters.length) {
        throw new BusinessError(HttpStatus.BAD_REQUEST, 'missingChapters', {missingChapters});
    }

    chapters.forEach(c =>authorize.update('chapter', c, req));
    
    const bulkData = chapterIds.map((id, order) => ({id, data: {order}}))
    let updatedChapters = await chapterModel.updateBulkChapters(bulkData);

    const filteredChapters = updatedChapters.map(p => authorize.read('chapter', p, req).filter(p));
    const resultChapters = await Promise.all(filteredChapters.map(prepareChapterForClient))

    res.send({items: resultChapters});
};

const deleteChapter = async (req, res) => {
    const {id} = req.params;

    const where = enforceAuthorization('chapter', req)
    const chapter = await chapterModel.getChapterById(id, {where});
    authorize.delete('chapter', chapter, req);
    
    await chapterModel.deleteChapter(id)
    
    res.send({id});
};


async function getChapters(req, res) {
    const {offset, limit, includeDeleted, orderBy, order, includes = '*'} = req.query;
    const {query = {}} = req.body;
    const {level} = req.params;

    let search = '';
    if (query.search) {
        const searchPart = {op: 'ilike', value: `%${query.search}%`}
        search = {$or: [{
            chapter: {name: searchPart},
        }]}
    }
    const where = {
        $and: [
            {$and: [{chapter: {level}}, {chapter: query.chapter}, search]},
            enforceAuthorization('chapter', req),
        ],
    }

    const {chapters, _meta} = await chapterModel.getChapters({limit, offset, where, orderBy, order, includeDeleted: includeDeleted === 'true', includes});

    const filteredChapters = chapters.map(p => authorize.read('chapter', p, req).filter(p));
    const resultChapters = await Promise.all(filteredChapters.map(prepareChapterForClient))

    res.send({_meta, items: resultChapters});
}

async function getChapter(req, res) {
    const {id} = req.params;

    const where = enforceAuthorization('chapter', req);
    const chapter = await chapterModel.getChapterById(id, {where});

    const filteredChapter = authorize.read('chapter', chapter, req).filter(chapter)
    const resultChapter = await prepareChapterForClient(filteredChapter);

    res.send(resultChapter);
}

export default (app) => {
    app.post('/level/:level/chapters/query', routeWrapper(getChapters));
    app.get('/chapters/:id', routeWrapper(getChapter));
    app.post('/level/:level/chapters', routeWrapper(addChapter));
    app.post('/chapters/reorder', routeWrapper(reorderChapters));
    app.patch('/chapters/:id', routeWrapper(updateChapter));
    app.delete('/chapters/:id', routeWrapper(deleteChapter));
};
