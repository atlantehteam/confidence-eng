import _ from 'lodash';
import { sendMail } from '../../services/mail';
import { getWelcomeTemplate, getUpdatePasswordTemplate } from '../../utils/mailUtils';
import {routeWrapper} from '../../utils/generalUtils'
import {authorize, enforceAuthorization} from '../../utils/rba'

import * as userModel from '../models/users'
import { generatePassword, hashPassword } from '../../utils/passUtils';

function validateUser(oldChapter, newUser) {
    const updatedUser = {...newUser};
    if (newUser.email) {
        updatedUser.email = newUser.email.toLowerCase();
    }
    return updatedUser;
}

const addUser = async (req, res) => {
    const {tenantId} = req.user;
    
    const validFields = req.body;
    
    const createPolicy = authorize.create('user', {...validFields, tenantId}, req);
    const userToSave = createPolicy.filter(validFields);
    
    const password = generatePassword();
    const hashedPass = await hashPassword(password);
    const validatedUser = validateUser(null, {...userToSave, tenantId, password, hashed_pass: hashedPass, role: 'student'});
    
    const dbUser = await userModel.addUser(validatedUser)
    const welcomeTemplate = getWelcomeTemplate(validatedUser)

    await sendMail({to: [validatedUser.email], html: welcomeTemplate, subject: 'קונפידנס - פרטי התחברות'})
    
    const readPolicy = authorize.read('user', dbUser, req);
    const result = readPolicy.filter(dbUser)

    res.send(result);
};

const updateUser = async (req, res) => {
    const {id} = req.params;
    
    const validFields = req.body;

    const where = enforceAuthorization('user', req)
    const user = await userModel.getUserById(id, {where});
    const updatePolicy = authorize.update('user', user, req);
    
    let validUser = updatePolicy.filter(validFields)

    if (validUser.generatePassword) {
        const password = generatePassword();
        const hashedPass = await hashPassword(password);
        validUser = {...validUser, password, hashed_pass: hashedPass};
    }
    validUser = validateUser(user, validUser);

    let updatedUser = await userModel.updateUser(id, validUser)
    
    if (validUser.generatePassword) {
        const resetPassTemplate = getUpdatePasswordTemplate({...updatedUser, password: validUser.password});
        await sendMail({to: [updatedUser.email], html: resetPassTemplate, subject: 'קונפידנס - איפוס סיסמה'})
    }

    const readPolicy = authorize.read('user', updatedUser, req);
    const result = readPolicy.filter(updatedUser);

    res.send(result);
};

const deleteUser = async (req, res) => {
    const {id} = req.params;

    const where = enforceAuthorization('user', req)
    const user = await userModel.getUserById(id, {where});
    authorize.delete('user', user, req);
    
    await userModel.deleteUser(id)
    
    res.send({id});
};

async function getUsers(req, res) {
    const {offset, limit, orderBy, order} = req.query;
    const {query = {}} = req.body;

    let search = '';
    if (query.search) {
        const searchPart = {op: 'ilike', value: `%${query.search.trim()}%`}
        search = {$or: [{
            user: {name: searchPart, email: searchPart},
        }]}
    }
    const where = {
        $and: [
            {$and: [{user: query.user}, search, {user: {role: 'student'}}]},
            enforceAuthorization('user', req),
        ],
    }

    const {users, _meta} = await userModel.getUsers({limit, offset, where, orderBy, order});

    const filteredUsers = users.map(p => authorize.read('user', p, req).filter(p));
    res.send({_meta, items: filteredUsers});
}

async function getUser(req, res) {
    const {id} = req.params;

    const where = enforceAuthorization('user', req);
    const user = await userModel.getUserById(id, {where});

    const filteredUser = authorize.read('user', user, req).filter(user)
    res.send(filteredUser);
}

export default (app) => {
    app.post('/users/query', routeWrapper(getUsers));
    app.get('/users/:id', routeWrapper(getUser));
    app.post('/users', routeWrapper(addUser));
    app.patch('/users/:id', routeWrapper(updateUser));
    app.delete('/users/:id', routeWrapper(deleteUser));
};
