import _ from 'lodash';
import BusinessError, { HttpStatus } from '../../utils/BusinessError';
import {routeWrapper} from '../../utils/generalUtils'
import {authorize, enforceAuthorization} from '../../utils/rba'

import * as mcQuestionModel from '../models/mcQuestions'

function validateMCQuestion(oldMCQuestion, newMCQuestion) {

    return newMCQuestion;
}

const addMCQuestion = async (req, res) => {
    const {tenantId} = req.user;
    const {chapterId} = req.params;
    const validFields = req.body;

    const createPolicy = authorize.create('mcQuestion', {...validFields, tenantId}, req);
    const mcQuestionToSave = createPolicy.filter(validFields);

    const validatedMCQuestion = validateMCQuestion(null, {...mcQuestionToSave, chapterId, tenantId});
    const dbMCQuestion = await mcQuestionModel.addMCQuestion(validatedMCQuestion)
    
    const readPolicy = authorize.read('mcQuestion', dbMCQuestion, req);
    const result = readPolicy.filter(dbMCQuestion)

    res.send(result);
};

const updateMCQuestion = async (req, res) => {
    const {id} = req.params;
    
    const validFields = req.body;

    const where = enforceAuthorization('mcQuestion', req)
    const mcQuestion = await mcQuestionModel.getMCQuestionById(id, {where});
    const updatePolicy = authorize.update('mcQuestion', mcQuestion, req);
    
    const validMCQuestion = updatePolicy.filter(validFields)
    const validatedMCQuestion = validateMCQuestion(mcQuestion, validMCQuestion);

    let updatedMCQuestion = await mcQuestionModel.updateMCQuestion(id, validatedMCQuestion)

    const readPolicy = authorize.read('mcQuestion', updatedMCQuestion, req);
    const result = readPolicy.filter(updatedMCQuestion);

    res.send(result);
};

const reorderMCQuestions = async (req, res) => {
    const {chapterId} = req.params;
    const mcQuestionIds = req.body;

    const where = {$and: [
        enforceAuthorization('mcQuestion', req),
        {mcQuestion: {id: {op: 'in', value: mcQuestionIds}, chapterId}},
    ]}
    const {mcQuestions} = await mcQuestionModel.getMCQuestions({where});
    const mcQuestionMap = _.keyBy(mcQuestions, 'id')
    const missingMCQuestions = mcQuestionIds.filter(chId => !mcQuestionMap[chId])
    if (missingMCQuestions.length) {
        throw new BusinessError(HttpStatus.BAD_REQUEST, 'missingMCQuestions', {missingMCQuestions});
    }

    mcQuestions.forEach(c => authorize.update('mcQuestion', c, req));
    
    const bulkData = mcQuestionIds.map((id, order) => ({id, data: {order}}))
    let updatedMCQuestions = await mcQuestionModel.updateBulkMCQuestions(bulkData);

    const filteredMCQuestions = updatedMCQuestions.map(p => authorize.read('mcQuestion', p, req).filter(p));
    res.send({items: filteredMCQuestions});
};


async function getMCQuestions(req, res) {
    const {offset, limit, includeDeleted, orderBy, order} = req.query;
    const {query = {}} = req.body;
    const {chapterId} = req.params;

    let search = '';
    if (query.search) {
        const searchPart = {op: 'ilike', value: `%${query.search}%`}
        search = {$or: [{
            mcQuestion: {title: searchPart, answer: searchPart},
        }]}
    }
    const where = {
        $and: [
            {$and: [{mcQuestion: {chapterId}}, {mcQuestion: query.mcQuestion}, search]},
            enforceAuthorization('mcQuestion', req),
        ],
    }

    const {mcQuestions, _meta} = await mcQuestionModel.getMCQuestions({limit, offset, where, orderBy, order, includeDeleted: includeDeleted === 'true'});

    const filteredMCQuestions = mcQuestions.map(p => authorize.read('mcQuestion', p, req).filter(p));
    res.send({_meta, items: filteredMCQuestions});
}

const deleteMCQuestion = async (req, res) => {
    const {id} = req.params;

    const where = enforceAuthorization('mcQuestion', req)
    const mcQuestion = await mcQuestionModel.getMCQuestionById(id, {where});
    authorize.delete('mcQuestion', mcQuestion, req);
    
    await mcQuestionModel.deleteMCQuestion(id)
    
    res.send({id});
};

export default (app) => {
    app.post('/chapters/:chapterId/mcQuestions', routeWrapper(addMCQuestion));
    app.post('/chapters/:chapterId/mcQuestions/query', routeWrapper(getMCQuestions));
    app.post('/chapters/:chapterId/mcQuestions/reorder', routeWrapper(reorderMCQuestions));
    app.delete('/mcQuestions/:id', routeWrapper(deleteMCQuestion));
    app.patch('/mcQuestions/:id', routeWrapper(updateMCQuestion));
};
