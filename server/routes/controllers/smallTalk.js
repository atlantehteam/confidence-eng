import _ from 'lodash';
import BusinessError, { HttpStatus } from '../../utils/BusinessError';
import {routeWrapper} from '../../utils/generalUtils'
import {authorize} from '../../utils/rba'
import {signUrls} from '../../utils/fileUtils';
import beginnerContent from '../units/smallTalk/beginner';
import intermediateContent from '../units/smallTalk/intermediate';
import advancedContent from '../units/smallTalk/advanced';

const allLevels = {
    welcome: {content: [
        {type: 'typography', text: 'איך לעבוד עם תוכנית "סמול טוק" - הצפיה חובה!', variant: 'h1', sx: {textAlign: 'center', mt: 2, color: 'primary.main'} },
        {type: 'iframe', url: 'https://player.vimeo.com/video/722565045?h=eaecde98a2'},
        {type: 'typography', text: 'איך להתמודד עם הפחד מלדבר אנגלית', variant: 'h2', sx: {textAlign: 'center', mt: 2} },
        {type: 'iframe', url: 'https://www.youtube.com/embed/uWZlG5cYp34'},
    ]},
}


const trainingSx = {color: 'primary', px: 4, mt: 2}
const htmlSx = {direction: 'rtl' /*flipped mui*/, lineHeight: 2}
const smallTalkPages = {
    beginner: {
        ...allLevels,
        checkIn: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/beginner-checkIn/audio/At check-in.mp3', sign: true},
                {type: 'html', html: beginnerContent.checkIn, sx: htmlSx},
                {type: 'buttonLink', title: 'תרגול אוצר מילים', url: 'https://quizlet.com/_bhvne0?x=1qqt&i=ln1na', sx: trainingSx},
            ],
        },
        park: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/beginner-park/audio/At The Park.mp3', sign: true},
                {type: 'html', html: beginnerContent.park, sx: htmlSx},
            ],
        },
        pool: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/beginner-pool/audio/At the Pool.mp3', sign: true},
                {type: 'html', html: beginnerContent.pool, sx: htmlSx},
            ],
        },
        howWasYourWeekend: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/beginner-howWasYourWeekend/audio/How was your weekend.mp3', sign: true},
                {type: 'html', html: beginnerContent.howWasYourWeekend, sx: htmlSx},
            ],
        },
        introductions: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/beginner-introductions/audio/introductions 1.mp3', sign: true},
                {type: 'html', html: beginnerContent.introductions, sx: htmlSx},
            ],
        },
        meetingTourist: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/beginner-meetingTourist/audio/Meeting a Tourist.mp3', sign: true},
                {type: 'html', html: beginnerContent.meetingTourist, sx: htmlSx},
            ],
        },
        onBus: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/beginner-onBus/audio/On the bus.mp3', sign: true},
                {type: 'html', html: beginnerContent.onBus, sx: htmlSx},
            ],
        },
        newColleague: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/beginner-newColleague/audio/the new colleague.mp3', sign: true},
                {type: 'html', html: beginnerContent.newColleague, sx: htmlSx},
            ],
        },
        spareTime: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/beginner-spareTime/audio/What do you like to do in your spare time.mp3', sign: true},
                {type: 'html', html: beginnerContent.spareTime, sx: htmlSx},
            ],
        },
    },
    intermediate: {
        ...allLevels,
        talkingAboutWork: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/intermediate-talkingAboutWork/audio/Talking about work 1.mp3', sign: true},
                {type: 'html', html: intermediateContent.talkingAboutWork, sx: htmlSx},
            ],
        },
        museum: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/intermediate-museum/audio/At The Museum.mp3', sign: true},
                {type: 'html', html: intermediateContent.museum, sx: htmlSx},
            ],
        },
        skiVacationInItaly: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/intermediate-skiVacationInItaly/audio/Ski vacation in Italy.mp3', sign: true},
                {type: 'html', html: intermediateContent.skiVacationInItaly, sx: htmlSx},
            ],
        },
        talkingAboutSports: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/intermediate-talkingAboutSports/audio/talking about sports.mp3', sign: true},
                {type: 'html', html: intermediateContent.talkingAboutSports, sx: htmlSx},
            ],
        },
        newColleague2: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/intermediate-newColleague2/audio/The New Colleague 2.mp3', sign: true},
                {type: 'html', html: intermediateContent.newColleague2, sx: htmlSx},
            ],
        },
        americanInTelAviv: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/intermediate-americanInTelAviv/audio/An American in Tel-Aviv.mp3', sign: true},
                {type: 'html', html: intermediateContent.americanInTelAviv, sx: htmlSx},
            ],
        },
        parisianInLondon: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/intermediate-parisianInLondon/audio/A Parisian in London.mp3', sign: true},
                {type: 'html', html: intermediateContent.parisianInLondon, sx: htmlSx},
            ],
        },
        getHealthy: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/intermediate-getHealthy/audio/Get Healthy.mp3', sign: true},
                {type: 'html', html: intermediateContent.getHealthy, sx: htmlSx},
            ],
        },
        travelingToDubai: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/intermediate-travelingToDubai/audio/Traveling to Dubai.mp3', sign: true},
                {type: 'html', html: intermediateContent.travelingToDubai, sx: htmlSx},
            ],
        },
        greetingsInvitations: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/intermediate-greetingsInvitations/audio/Greetings and Invitations.mp3', sign: true},
                {type: 'html', html: intermediateContent.greetingsInvitations, sx: htmlSx},
            ],
        },
    },
    advanced: {
        ...allLevels,
        firstMeeting: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/advanced-firstMeeting/audio/First Meeting .mp3', sign: true},
                {type: 'html', html: advancedContent.firstMeeting, sx: htmlSx},
            ],
        },
        trafficChaos: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/advanced-trafficChaos/audio/Traffic Chaos.mp3', sign: true},
                {type: 'html', html: advancedContent.trafficChaos, sx: htmlSx},
            ],
        },
        executivesMeet: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/advanced-executivesMeet/audio/Executives Meet.mp3', sign: true},
                {type: 'html', html: advancedContent.executivesMeet, sx: htmlSx},
            ],
        },
        sustainableEnergy: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/advanced-sustainableEnergy/audio/Sustainable Energy.mp3', sign: true},
                {type: 'html', html: advancedContent.sustainableEnergy, sx: htmlSx},
            ],
        },
        friendAirport: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/advanced-friendAirport/audio/A Friend at the Airport.mp3', sign: true},
                {type: 'html', html: advancedContent.friendAirport, sx: htmlSx},
            ],
        },
        lookingForApartment: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/advanced-lookingForApartment/audio/Looking for an Apartment.mp3', sign: true},
                {type: 'html', html: advancedContent.lookingForApartment, sx: htmlSx},
            ],
        },
        troubleInNorth: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/advanced-troubleInNorth/audio/Trouble in the North.mp3', sign: true},
                {type: 'html', html: advancedContent.troubleInNorth, sx: htmlSx},
            ],
        },
        onlineSales: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/advanced-onlineSales/audio/Online Sales.mp3', sign: true},
                {type: 'html', html: advancedContent.onlineSales, sx: htmlSx},
            ],
        },
        positionsHq: {
            content: [
                {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/advanced-positionsHq/audio/Positions at HQ.mp3', sign: true},
                {type: 'html', html: advancedContent.positionsHq, sx: htmlSx},
            ],
        },
    },
}

async function getSmallTalkPage(req, res) {
    const {smallTalkPage, level} = req.params;

    const page = smallTalkPages[level][smallTalkPage];
    if (!page) {
        throw new BusinessError(HttpStatus.NOT_FOUND, 'pageNotFound');
    }
    const filteredPage = authorize.read('smallTalk', 'smallTalk', req).filter(page)
    filteredPage.content = await signUrls(filteredPage.content)
    res.send(filteredPage);
}

export default (app) => {
    app.get('/pages/smallTalk/:level/:smallTalkPage', routeWrapper(getSmallTalkPage));
};
