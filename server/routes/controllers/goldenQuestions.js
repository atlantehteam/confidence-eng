import _ from 'lodash';
import BusinessError, { HttpStatus } from '../../utils/BusinessError';
import { signFileUrl } from '../../utils/fileUtils';
import {routeWrapper} from '../../utils/generalUtils'
import {authorize, enforceAuthorization} from '../../utils/rba'

import * as goldenQuestionModel from '../models/goldenQuestions'

function validateGoldenQuestion(oldGoldenQuestion, newGoldenQuestion) {

    return newGoldenQuestion;
}

async function prepareQuestionForClient(question) {
    const updatedQuestion = {...question};

    const propsToSign = ['answerAudio'];
    const promises = propsToSign.map(async prop => {
        if (!question[prop]) {return}
        const signedUrl = await signFileUrl(question[prop])
        updatedQuestion[prop] = signedUrl;
    })
    
    await Promise.all(promises)
    
    return updatedQuestion;
}

const addGoldenQuestion = async (req, res) => {
    const {tenantId} = req.user;
    const {chapterId} = req.params;
    const validFields = req.body;

    const createPolicy = authorize.create('goldenQuestion', {...validFields, tenantId}, req);
    const goldenQuestionToSave = createPolicy.filter(validFields);

    const validatedGoldenQuestion = validateGoldenQuestion(null, {...goldenQuestionToSave, chapterId, tenantId});
    const dbGoldenQuestion = await goldenQuestionModel.addGoldenQuestion(chapterId, validatedGoldenQuestion)
    
    const readPolicy = authorize.read('goldenQuestion', dbGoldenQuestion, req);
    const filtered = readPolicy.filter(dbGoldenQuestion)
    const result = await prepareQuestionForClient(filtered);
    
    res.send(result);
};

const updateGoldenQuestion = async (req, res) => {
    const {id} = req.params;
    
    const validFields = req.body;

    const where = enforceAuthorization('goldenQuestion', req)
    const goldenQuestion = await goldenQuestionModel.getGoldenQuestionById(id, {where});
    const updatePolicy = authorize.update('goldenQuestion', goldenQuestion, req);
    
    const validGoldenQuestion = updatePolicy.filter(validFields)
    const validatedGoldenQuestion = validateGoldenQuestion(goldenQuestion, validGoldenQuestion);

    let updatedGoldenQuestion = await goldenQuestionModel.updateGoldenQuestion(id, goldenQuestion.chapterId, validatedGoldenQuestion)

    const readPolicy = authorize.read('goldenQuestion', updatedGoldenQuestion, req);
    const filtered = readPolicy.filter(updatedGoldenQuestion);
    const result = await prepareQuestionForClient(filtered);

    res.send(result);
};

const reorderGoldenQuestions = async (req, res) => {
    const {chapterId} = req.params;
    const goldenQuestionIds = req.body;

    const where = {$and: [
        enforceAuthorization('goldenQuestion', req),
        {goldenQuestion: {id: {op: 'in', value: goldenQuestionIds}, chapterId}},
    ]}
    const {goldenQuestions} = await goldenQuestionModel.getGoldenQuestions({where});
    const goldenQuestionMap = _.keyBy(goldenQuestions, 'id')
    const missingGoldenQuestions = goldenQuestionIds.filter(chId => !goldenQuestionMap[chId])
    if (missingGoldenQuestions.length) {
        throw new BusinessError(HttpStatus.BAD_REQUEST, 'missingGoldenQuestions', {missingGoldenQuestions});
    }

    goldenQuestions.forEach(c => authorize.update('goldenQuestion', c, req));
    
    const bulkData = goldenQuestionIds.map((id, order) => ({id, data: {order}}))
    let updatedGoldenQuestions = await goldenQuestionModel.updateBulkGoldenQuestions(bulkData);

    const filteredGoldenQuestions = updatedGoldenQuestions.map(p => authorize.read('goldenQuestion', p, req).filter(p));
    const resultItems = await Promise.all(filteredGoldenQuestions.map(prepareQuestionForClient))

    res.send({items: resultItems});
};


async function getGoldenQuestions(req, res) {
    const {offset, limit, includeDeleted, orderBy, order} = req.query;
    const {query = {}} = req.body;
    const {chapterId} = req.params;

    let search = '';
    if (query.search) {
        const searchPart = {op: 'ilike', value: `%${query.search}%`}
        search = {$or: [{
            goldenQuestion: {title: searchPart, answer: searchPart},
        }]}
    }
    const where = {
        $and: [
            {$and: [{goldenQuestion: {chapterId}}, {goldenQuestion: query.goldenQuestion}, search]},
            enforceAuthorization('goldenQuestion', req),
        ],
    }

    const {goldenQuestions, _meta} = await goldenQuestionModel.getGoldenQuestions({limit, offset, where, orderBy, order, includeDeleted: includeDeleted === 'true'});

    const filteredGoldenQuestions = goldenQuestions.map(p => authorize.read('goldenQuestion', p, req).filter(p));
    const resultItems = await Promise.all(filteredGoldenQuestions.map(prepareQuestionForClient))

    res.send({_meta, items: resultItems});
}

const deleteGoldenQuestion = async (req, res) => {
    const {id} = req.params;

    const where = enforceAuthorization('goldenQuestion', req)
    const goldenQuestion = await goldenQuestionModel.getGoldenQuestionById(id, {where});
    authorize.delete('goldenQuestion', goldenQuestion, req);
    
    await goldenQuestionModel.deleteGoldenQuestion(id)
    
    res.send({id});
};

export default (app) => {
    app.post('/chapters/:chapterId/goldenQuestions', routeWrapper(addGoldenQuestion));
    app.post('/chapters/:chapterId/goldenQuestions/query', routeWrapper(getGoldenQuestions));
    app.post('/chapters/:chapterId/goldenQuestions/reorder', routeWrapper(reorderGoldenQuestions));
    app.delete('/goldenQuestions/:id', routeWrapper(deleteGoldenQuestion));
    app.patch('/goldenQuestions/:id', routeWrapper(updateGoldenQuestion));
};
