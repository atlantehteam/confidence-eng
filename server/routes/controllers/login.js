import _ from 'lodash'
import HttpStatus from 'http-status-codes'
import knex from '../../services/knex'
import BusinessError from '../../utils/BusinessError'
import {routeWrapper} from '../../utils/generalUtils'
import { generateAccessToken } from '../../utils/jwt'
import { checkPassword } from '../../utils/passUtils'

const login = async (req, res) => {
    const {username, password} = req.body;
    if (!username || !password) {
      throw new BusinessError(HttpStatus.BAD_REQUEST, 'invalidUserPass');
    }
    const [user] = await knex('users')
        .where({email: username.toLowerCase().trim()})
        .select(['id', 'hashed_pass', 'tenantId', 'name', 'role', 'email', 'permissions']).limit(1);
    if (user) {
        const isMatch = await checkPassword(password, user.hashed_pass);
        if (isMatch) {
            const jwtPayload = {..._.pick(user, ['name', 'id', 'email', 'tenantId', 'permissions']), primaryRole: user.role};
            const token = generateAccessToken(jwtPayload)
            return res.json({access_token: token, ...jwtPayload});
        }
    }
    return res.status(HttpStatus.UNAUTHORIZED).send({key: 'noMatchUserPass'})
}

export default (app) => {
    app.post('/auth/login', routeWrapper(login));
};
