import _ from 'lodash';
import {routeWrapper} from '../../utils/generalUtils'
import * as userModel from '../models/users';
import BusinessError, { HttpStatus } from '../../utils/BusinessError';
import { generatePassword, hashPassword } from '../../utils/passUtils';
import config from '../../config/config';
import { getNewPermissionsTemplate, getWelcomeTemplate } from '../../utils/mailUtils';
import { sendMail } from '../../services/mail';
import { permissionKeys } from '../../utils/rba/permissions';

const updateStudentPermissions = async (req, res) => {
    const {name, email, addedPermissions} = req.body;
    
    if (!email || !name || !addedPermissions || !addedPermissions.length) {
        throw new BusinessError(HttpStatus.BAD_REQUEST, 'missingFields', {requiredFields: ['email', 'name', 'addedPermissions']});
    }
    
    const invalidPermissions = _.difference(addedPermissions, permissionKeys);
    if (invalidPermissions.length) {
        throw new BusinessError(HttpStatus.BAD_REQUEST, 'invalidPermissions', {invalidPermissions})
    }
    const lowerCasedEmail = email.toLowerCase();
    const {users: [user]} = await userModel.getUsers({where: {user: {email: lowerCasedEmail}}, limit: 1})
    
    if (!user) {
        const password = generatePassword();
        const hashedPass = await hashPassword(password);
        const userToAdd = {email: lowerCasedEmail, name, tenantId: req.user.tenantId, password, hashed_pass: hashedPass, role: 'student', selectedLevel: 'beginner', permissions: _.uniq(addedPermissions)};
        
        await userModel.addUser(userToAdd)
        
        const welcomeTemplate = getWelcomeTemplate(userToAdd)
        await sendMail({to: [userToAdd.email], html: welcomeTemplate, subject: 'קונפידנס - פרטי התחברות'})
    } else if (user.tenantId === config.service.defaultTenant && user.tenantId !== req.user.tenantId) {
        throw new BusinessError(HttpStatus.BAD_REQUEST, 'userDowngradeForbidden')
    } else {
        const permissionsSet = new Set(user.permissions);
        addedPermissions.forEach(p => permissionsSet.add(p));
        const userToUpdate = {permissions: Array.from(permissionsSet), tenantId: req.user.tenantId}
        await userModel.updateUser(user.id, userToUpdate);

        const updatePermissionsTemplate = getNewPermissionsTemplate({...user, ...userToUpdate})
        await sendMail({to: [user.email], html: updatePermissionsTemplate, subject: 'קונפידנס - הרשאות חדשות במערכת'})
    }
    res.status(HttpStatus.CREATED).send();
}

export default (app) => {
    app.patch('/integration/students', routeWrapper(updateStudentPermissions));
};
