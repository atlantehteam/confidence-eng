import BusinessError from '../utils/BusinessError'

export default (app) => {
    // 404 - Not Found
    app.use((req, res) => {
        console.error(`Not Found (404) - (${req.method}) ${req.url}`);
        res.status(404).send({code: 404, msg: 'Not Found'});
    });

    const shortenStrings = (key, value) => {
        if (typeof value === 'string' && value.length > 200) {
            return `${value.slice(0, 200)}...`;
        }
        return value;
    }

    app.use((err, req, res, next) => {
        const user = req.user ? `${req.user.email} (${req.user.id})` : 'guest';
        console.info(`Error has occured to user: ${user}`);
        console.info(`Payload: ${JSON.stringify(req.body, shortenStrings)}`);
        const logErr = err instanceof BusinessError ? console.log : console.error;

        logErr(err instanceof Error ? err : JSON.stringify(err));
        if (res.headersSent) {
            console.log('headers sent. will not send error to client');
            return next(err)
        }
        const responseError = err instanceof BusinessError ? err : new BusinessError();
        const {key, meta} = responseError;
        res.status(responseError.code).send({key, meta});
    });
};
