# Confidence - Documentation
This is an overview of the technology and structure of the "Confidence" project.
This project attempts to follow the [12 factor principal](https://12factor.net/config) as much as possible and keep all production configuration out of code.
The server does have some development config files checked in to ease on development, but these files should never be shared or used in production environment.

The project uses `yarn` as the main package manager and scripts runner.  
The project is run locally in WSL2 ubuntu 2020.04

# Server
The server is written in nodejs, and uses postgres as its primary database.
When the server loads it attempts to read environment files based on the environment the server is running on.
For example, for dev environment, the server will attempt to load environment variables from the file `env/server.dev.env`. In production, on the other hand, the environment variables are injected by the deployment.

In order to abide by the [12 factor principal](https://12factor.net/config) the following required files are excluded from source code and should be created locally by the developer:
```
env/gcp-service-account-key.json - This code holds the developer service account key to access GCP resources
```

## structure
The server structure is built as following
```
server
-- index.js - the main server file
-- routes
---- index.js - aggregates all routes, and adds validation middlewares
---- controllers - controllers folder. Controllers hold the route logic
---- models - models folder. Models hold the db access logic 
-- utils - various utilities
---- rba - the role based access & permissions logic is placed here.
```

## migrations
Before running the server locally, you must have postgres ready to use, that has the credentials specified in `env/server.dev.env`.
After you install postgres, you need to run db migrations to have all the db tables prepared.
In order to to that. you need to run the following commands:  
```
cd server
yarn migrate:dev
```

To add a new migration run the following command inside `server`:
```
yarn createMigration <your_new_migration_name>
```

There are some more handy scripts in `server/package.json` that you can use.

# client
The client is written in React and has these libraries the basic barebone of the app: Redux, Redux-Saga, MUI.  
The client has `modules` structure to help separate view logic from business logic:
```
client/src
-- index.js - the main client file that holds the wrapper components and app render code.
-- components - Holds all "dumb" reusable components that don't generatlly interact with business logic
-- pages - Holds all pages and top level components that DO interact with business logic
---- actions - All the actions that pages can dispatch
-- services - Very thin wrapper over some external services used in the app
-- modules - All the business logic of the app lies here.
---- index.js - aggregates all modules.
---- <some modules>
------ actions - the actions that this module dispatches
------ reducer - the reducer of this modules that holds all the actions and outcomes for this module.
------ sagas - the actions performed as a concequence of dispatched page actions
------ selectors - partial state selectors for this module to be used by the view.
-- utils - various utilities
```

# terraform and deployment
The project is deployed to GCP using terraform.  
In order to abide by the [12 factor principal](https://12factor.net/config) some required files are excluded from source code and should be created locally by the developer:
```
dev.tfvars.json - Used to deploy into dev env
prod.tfvars.json - Used to deploy into dev env

Their structure looks somewhat like:
{
    "RN_SMTP_HOST": "<fill in by developer>",
    "RN_SMTP_PORT": "<fill in by developer>",
    "RN_SMTP_USER": "<fill in by developer>",
    "RN_SMTP_PASS": "<fill in by developer>""
}
```
In order to deploy the code to some environment, there are some handy scripts in `package.json`:
```
yarn ship:dev - Builds server and client and deploys them into DEV env
yarn ship:prod - Builds server and client and deploys them into PROD env
```

## client deployment
Since Client is deployed to firebase, which has its own CLI, the client is not deployed using terraform. Instead, after terraform is called, firebase is called as well to deploy the client side code

## cloudflare
In order to run terraform and to update records in cloudflare, you need to have cloudflare API token. Generate it on cloudflare console and inejct it into your terminal under env variable called `CLOUDFLARE_API_TOKEN`  
To ease support for multiple projects, there's a `.vscode/settings.json` file that injects `WS_PROFILE` env variable to the terminal with this project's name. You can add a small method in your `~/.bashrc` to set it up for you:
```
set_workspace() {
    echo "Setting workspace profile: $1"
    case $1 in
    confidence)
        export CLOUDFLARE_API_TOKEN=<Your Token Here>
    ;;
    *)
    echo "Invalid profile $1"
    ;;
    esac
}

if [[ -n $WS_PROFILE ]]; then
   set_workspace $WS_PROFILE
fi
echo $WS_PROFILE
```