import fs from 'node:fs/promises';
import readline from 'readline';
import {google} from 'googleapis';
import _ from 'lodash';
import axios from 'axios';
import path from 'node:path';

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/drive.readonly'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = '../env/token.json';

async function authorize() {
    const content = await fs.readFile('../env/gcp_client_secret.json')
    const secret = JSON.parse(content)
    const oAuth2Client = new google.auth.OAuth2(
        secret.installed.client_id,
        secret.installed.client_secret,
        `urn:ietf:wg:oauth:2.0:oob`);
  
    // Check if we have previously stored a token.
    let token;
    try {
      const tokenStr = await fs.readFile(TOKEN_PATH, 'utf-8')
      token = JSON.parse(tokenStr);
    } catch (err) {
      token = await getNewToken(oAuth2Client);
    }
    oAuth2Client.setCredentials(token);
    return oAuth2Client;
  }
  
  /**
   * Get and store new token after prompting for user authorization, and then
   * execute the given callback with the authorized OAuth2 client.
   * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
   * @param {getEventsCallback} callback The callback for the authorized client.
   */
  function getNewToken(oAuth2Client) {
    return new Promise((res, reject) => {
      const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
      });
      console.log('Authorize this app by visiting this url:', authUrl);
      const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
      });
      rl.question('Enter the code from that page here: ', (code) => {
        rl.close();
        oAuth2Client.getToken(code, (err, token) => {
          if (err) {
            console.error('Error retrieving access token', err)
            reject(err);
          } else {
            // Store the token to disk for later program executions
            fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
              if (err) return reject(err);
              console.log('Token stored to', TOKEN_PATH);
            });
            res(token);
          }
        });
      });
    })
  }

async function listFolderFiles(drive, parent) {
    const res = await drive.files.list({
        pageSize: 20,
        q: parent && `'${parent}' in parents`,
        fields: 'nextPageToken, files(id, name, mimeType, trashed, webContentLink)',
    });
    const {files} = res.data;


    const promises = await files.map(async f => {
        if (f.mimeType === 'application/vnd.google-apps.folder') {
            const folderFiles = await listFolderFiles(drive, f.id);
            return {...f, files: folderFiles}
        }
        return f;
    })
    return Promise.all(promises);
}

async function generateFilesStructure() {
    try {
        const auth = await authorize();
        const drive = google.drive({version: 'v3', auth});
    
        const structure = await listFolderFiles(drive, '1XbTbqttZata_3YB5Y79xByGmpWeq6NA2')

        await fs.writeFile('importSmallTalk/temp/smallTalkContent.json', JSON.stringify(structure, null, 2));

        console.log(topFolders)
    } catch (err) {
        console.error(err);
    }
    
}

async function downloadSingleFile(file) {
    switch (file.mimeType) {
    case 'audio/mpeg':
        return file;
        break;
    case 'text/plain':
        const res = await drive.files.export({fileId: '1L18h3boQsg7pvkm88hNmWZ1ahgPDlcbDdXeO0RzOJJY', mimeType: "text/plain"})
        return {...file, textContent: res.data}
    default:
        throw new Error(`Invalid mimetype: ${file.mimeType}`);
    }
}

async function downloadFiles(files) {
    const promises = files.map(async file => {
        if (file.mimeType === 'application/vnd.google-apps.folder') {
            const downloadedFiles = await downloadFiles(file.files);
            return {
                ...file,
                files: downloadedFiles,
            }
        }
        return downloadSingleFile(file);
    })

    return Promise.all(promises);
}

// async function readFiles() {
//     const auth = await authorize();
//     const drive = google.drive({version: 'v3', auth});

//     const strFile = await fs.readFile('importSmallTalk/temp/smallTalkContent.json', 'utf-8');
//     const structure = JSON.parse(strFile);

//     const res = await drive.files.get({fileId: '19lEavzXcYDLGMJ73EDQpyrlq9zD5kwyX', fields: '*'})
//     // const res = await drive.files.export({fileId: '1L18h3boQsg7pvkm88hNmWZ1ahgPDlcbDdXeO0RzOJJY', mimeType: "text/plain"})

//     drive.files.get({alt: 'media'}).pipe()
//     console.log(res.data);
// }

async function uploadMediaAndGenerateModule() {

    const auth = await authorize();
    const drive = google.drive({version: 'v3', auth});

    const tokenStr = await fs.readFile(TOKEN_PATH, 'utf-8')
    const token = JSON.parse(tokenStr);
      
    const strFile = await fs.readFile('importSmallTalk/temp/smallTalkContent.json', 'utf-8');
    const structure = JSON.parse(strFile);
    const levelMap = {Elem: 'beginner', Interm: 'intermediate', Advanced: 'advanced'}
    
    const level = structure[1];
    const levelVal = levelMap[level.name];
    const audioFolder = level.files.find(f => f.mimeType === 'application/vnd.google-apps.folder')
    const textFiles = level.files.filter(f => f.mimeType === 'text/plain')
    const audioFiles = audioFolder.files.filter(f => f.mimeType === 'audio/mpeg')

    const populatedTextFiles = await Promise.all(textFiles.map(async tf => {
        const res = await drive.files.get({alt: 'media', fileId: tf.id})
        return {...tf, textContent: res.data}
    }))

    const chapterSectionsStr = _.sortBy(populatedTextFiles, f => parseInt(f.name)).reduce((str, tf) => {
        const match = tf.name.match(/(\d)+ (.*)/)
        const num = match[1]
        const name = match[2]
        const chapterSlug = _.camelCase(name.replace(/(\ba\b|\bat\b|\bthe\b|\ban\b|\band\b|what do you like to do in your|\.mp3|\.txt)/ig, ''));
        const result = tf.textContent
            .trim()
            .replace(/\r+/g, "")
            .replace(/\n+/g, "\n")
            .replace(/.*?\n/, '\n')
            .replace(/[’‘]/g, "'")
            .replace(/'/g, "\\'")
            .replace(/[ \t]+/g, ' ')
            .replace(/\n/g, '\n\'')
            .replace(/'(.*?:)/g, '\'<b>$1</b>')
            .replace(/\n/g, '\',\n')
            .replace(/',\n/, '')
            .replace(/''/, '')
        return `${str}    ${chapterSlug}: [\n${result}',\n].join('<br />'),\n`
    }, '')

    const fileContent = `export default {\n${chapterSectionsStr}\n}\n`;

    await fs.writeFile(`../server/routes/units/smallTalk/${levelVal}.js`, fileContent);
    console.log(fileContent)
    // const levelContent = textFiles.reduce((aggr, tf) => {
    //     const match = tf.name.match(/(\d)+ (.*)/)
    //     const num = match[1]
    //     const name = match[2]
    //     const chapterSlug = _.camelCase(name.replace(/(\ba\b|\bat\b|\bthe\b|\ban\b|\band\b|what do you like to do in your|\.mp3|\.txt)/ig, ''));
        
    //     aggr[chapterSlug] = {
    //         content: [
    //             {type: 'html', html: tf.textContent},
    //         ]
    //     }
    // }, {})
    // const mappedFiles = audioFiles.map(file => _.camelCase(file.name.replace(/(\ba\b|\bat\b|\bthe\b|\ban\b|\band\b|what do you like to do in your|\.mp3)/ig, '')));
    
    // console.log(mappedFiles);
    // return {
    //     beginner: {
    //         checkin: {
    //             content: [
    //                 {type: 'audio', url: 'gs://confidence-eng-shared/modules/smallTalk/beginner-checkin/audio/At check-in.mp3', sign: true},
    //                 {type: 'html', html: beginnerContent.checkin, sx: htmlSx},
    //                 {type: 'buttonLink', title: 'תרגול אוצר מילים', url: 'https://quizlet.com/_bhvne0?x=1qqt&i=ln1na', sx: trainingSx},
    //             ],
    //         },
    //     }
    // }
}
// generateFilesStructure();

// uploadMediaAndGenerateModule();

async function generateFolderStructure() {
  const strFile = await fs.readFile('importSmallTalk/temp/smallTalkContent.json', 'utf-8');
  const structure = JSON.parse(strFile);
  const levelMap = {Elem: 'beginner', Interm: 'intermediate', Advanced: 'advanced'}

  const level = structure[0];
  const levelVal = levelMap[level.name];
  const folder = '/mnt/c/Users/Roi/Desktop/all/confidence/audio/smalltalk/Advanced Audios'
  const files = await fs.readdir(folder);

  level.files.slice(1).map(async f => {
    const match = f.name.match(/(\d)+ (.*)/)
    const num = match[1]
    const name = match[2].slice(0, -4)
    const chapterSlug = _.camelCase(name.replace(/(\ba\b|\bat\b|\bthe\b|\ban\b|\band\b|what do you like to do in your|\.mp3|\.txt)/ig, ''));
    const file = files.find(f => f.toLowerCase().includes(name.toLowerCase()));
    if (!file) {
      throw new Error('missing file ' + name)
    }

    const newFolder = `${folder}/${levelVal}-${chapterSlug}/audio`;
    await fs.mkdir(newFolder, {recursive: true})
    await fs.copyFile(`${folder}/${file}`, `${newFolder}/${file}`)

  })
}

generateFolderStructure();

