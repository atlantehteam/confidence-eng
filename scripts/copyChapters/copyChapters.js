import createKnex from 'knex';
import _ from 'lodash';

const knex = createKnex(({
    client: 'pg',
    asyncStackTraces: true,
    connection: {
        host : process.env.KNEX_HOST,
        port : process.env.KNEX_PORT,
        user : process.env.KNEX_USER,
        password : process.env.KNEX_PASSWORD,
        database : process.env.KNEX_DB,
        timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
        ssl: false,
    },
    pool: {
        min: 0,
        max: 10,
    },
}))

const chaptersToCopy = [
    '616ff2a0-6759-4861-9cf2-a35be437659f', // intro
    '22222222-1111-1111-1111-111111111001', // beginner chapter 1
    '22222222-1111-1111-1111-111111111002', // beginner chapter 2
    '22222222-1111-1111-1111-111111111103', // intermediate chapter 3
    '22222222-1111-1111-1111-111111111110', // intermediate chapter 10
    '22222222-1111-1111-1111-111111111216', // advanced chapter 16
]

const chapters = await knex('chapters').whereIn('id', chaptersToCopy);
const existingChapters = await knex('chapters').where({'tenantId': 222222});
// console.log(chapters);

const chaptersToUpsert = chapters.map((c) => ({
    ..._.omit(c, ['id', 'tenantId']),
    id: existingChapters.find(a => a.comments === c.id)?.id,
    comments: c.id,
    tenantId: 222222,
}))

await knex('chapters').insert(chaptersToUpsert).onConflict('id').ignore();

const newChapters = await knex('chapters').whereIn('comments', chaptersToCopy);

const promises = newChapters.slice(1).map(async chapter => {
    const goldenQuestions = await knex('golden_questions').where({chapterId: chapter.comments})
    const mcQuestions = await knex('mc_questions').where({chapterId: chapter.comments})
    
    const newGoldenQuestions = goldenQuestions.map(d => ({
        ..._.omit(d, ['id', 'createdAt', 'updatedAt']),
        chapterId: chapter.id,
        tenantId: 222222,
        answerAudio: null,
    }))
    const newMCQuestions = mcQuestions.map(d => ({
        ..._.omit(d, ['id', 'createdAt', 'updatedAt']),
        chapterId: chapter.id,
        tenantId: 222222,
    }))
    
    if (newGoldenQuestions.length) {
        await knex('golden_questions').insert(newGoldenQuestions)
    }
    if (newMCQuestions.length) {
        await knex('mc_questions').insert(newMCQuestions)
    }
})

await Promise.all(promises)
