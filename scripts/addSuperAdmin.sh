# Should be run locally on production using production env
# set -e;

# SALT=$(openssl rand -base64 12)
# DOCKER_ID=$(docker ps -qf "name=^technypro-db")
# if [ -z $DOCKER_ID ] || [ -z $EE_DEFAULT_TENANT ] ;
# then
#     echo 'DB Container is down or not default tenant'
#     exit 1
# fi

# echo "DB Docker Container: ${DOCKER_ID}"
# read -p "Enter superAdmin name: " adminName
# read -p "Enter superAdmin email: " adminEmail
# read -p "Enter superAdmin nationalId: " adminNationalId
# read -p "Set a password (blank for no pass): " adminPass

# HASHED_PASS=$(echo -n "${adminPass}" | argon2 ${SALT} -e)

# docker exec -it ${DOCKER_ID} psql -U postgres -d $KNEX_DB -c \
#     "insert into users(\"tenantId\",   \"name\",   \"email\", \"hashed_pass\", \"role\", \"nationalId\") \
#         values('${EE_DEFAULT_TENANT}', '${adminName}', '${adminEmail}', '${HASHED_PASS}', 'superAdmin', '${adminNationalId}');"