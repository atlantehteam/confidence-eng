######
# MUST USE 127.0.0.1 in pgadmin or you might get connection issues
######

if [ -z "${RN_DEPLOYMENT_ENV}" ]; then
  echo "Missing RN_DEPLOYMENT_ENV"
  exit 1
fi

{
  sleep 5m
  echo "@@@ Timing out @@@"
  kill $$
} &

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")

gcloud secrets versions access latest --secret ec-$RN_DEPLOYMENT_ENV-db-credentials --project confidence-eng  | jq

CLOUDSQL_CONNECTION_NAME=$(gcloud sql instances list --project confidence-eng --format="value(connectionName)" --filter="labels.environment=${RN_DEPLOYMENT_ENV}")

echo Using SQL Instance: ${CLOUDSQL_CONNECTION_NAME}

CLOUDSQL_CONNECTION_NAME=${CLOUDSQL_CONNECTION_NAME} docker-compose -f ${SCRIPTPATH}/docker-compose.sql.yml up

exit 0

# windows:
for /F %n IN ('gcloud sql instances list --project confidence-eng --filter="name:prod" --format="csv[no-heading](name)"') DO @(set instance=%n)
docker run -it --rm -v %userprofile%\AppData\Roaming\gcloud:/config -e GOOGLE_APPLICATION_CREDENTIALS=/config/application_default_credentials.json -p 127.0.0.1:5434:5432 gcr.io/cloudsql-docker/gce-proxy:1.19.1 /cloud_sql_proxy -instances=confidence-eng:europe-west1:%INSTANCE%=tcp:0.0.0.0:5432