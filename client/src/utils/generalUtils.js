export const delay = (millis) => new Promise(res => setTimeout(res, millis))
