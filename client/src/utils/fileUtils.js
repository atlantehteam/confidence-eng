export const fileToBase64 = async(file) => {
	if (!file) {throw new Error(`Missing file`)};

	return new Promise(res => {
		const reader = new FileReader();
		reader.onloadend = () => {
			res(reader.result)
		};
		reader.readAsDataURL(file);
	});
}