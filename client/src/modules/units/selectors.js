import _ from 'lodash';
import { createSelector } from 'reselect';

export const sliceSelector = (state) => state.units;
export const selectedPage = createSelector(sliceSelector, slice => slice.selectedPage);
export const loaders = createSelector(sliceSelector, slice => slice.loaders);
export const justSubmitted = createSelector(sliceSelector, slice => !!slice.justSubmitted);
export const submitError = createSelector(sliceSelector, slice => slice.submitError);
