import _ from 'lodash';
import { createSelector } from 'reselect';
import * as NavSelectors from 'modules/navigation/selectors';

export const sliceSelector = (state) => state.users;
export const users = createSelector(sliceSelector, slice => slice.users || []);
export const usersTotal = createSelector(sliceSelector, slice => slice.usersTotal || 0);
export const selectedChapter = createSelector(sliceSelector, slice => slice.selectedChapter);
export const loaders = createSelector(sliceSelector, slice => slice.loaders);
export const justSubmitted = createSelector(sliceSelector, slice => !!slice.justSubmitted);
export const submitError = createSelector(sliceSelector, slice => slice.submitError);

export const filter = createSelector(sliceSelector, slice => slice.filter);

export const matchingChapterId = createSelector(
	NavSelectors.pathnameSelector,
	(pathname) => {
		const matchedRegex = (pathname || '').match(/\/users\/([^/?]*)/) || [];
		const id = matchedRegex[1];
		return parseInt(id);
	}
);
