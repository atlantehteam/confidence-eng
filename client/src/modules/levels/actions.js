import {createActionTypes, createActionCreators} from 'utils/reduxUtils';

const typesArray = [

	'FETCH_LEVEL_CHAPTERS_REQUEST',
	'FETCH_LEVEL_CHAPTERS_SUCCESS',
	'FETCH_LEVEL_CHAPTERS_FAILURE',
];

export const ActionTypes = createActionTypes(typesArray);
export const Actions = createActionCreators(typesArray);
