import _ from 'lodash';
import { createSelector } from 'reselect';

export const sliceSelector = (state) => state.questions;
export const goldenQuestions = createSelector(sliceSelector, slice => slice.goldenQuestions || []);
export const mcQuestions = createSelector(sliceSelector, slice => slice.mcQuestions || []);
export const loaders = createSelector(sliceSelector, slice => slice.loaders);
export const justSubmitted = createSelector(sliceSelector, slice => !!slice.justSubmitted);
export const submitError = createSelector(sliceSelector, slice => slice.submitError);
