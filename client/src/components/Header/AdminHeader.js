import React, { useState } from "react";
import {
	AppBar as MuiAppBar,
	Toolbar,
	IconButton,
	Menu,
	MenuItem,
	Box,
	Typography,
	Autocomplete,
	TextField,
} from '@mui/material';
import {
	Menu as MenuIcon,
	Person as AccountIcon,
	ArrowForward as ArrowForwardIcon,
	ExitToApp as ExitToAppIcon,
} from "@mui/icons-material";
import { styled } from '@mui/material/styles';
import * as ChapterSelectors from 'modules/chapters/selectors';
import * as LevelSelectors from 'modules/levels/selectors';
import * as AuthSelectors from 'modules/auth/selectors';
import {Actions} from 'pages/actions';

// styles
import styles from "./styles";

// context
import {
	useLayoutState,
	useLayoutDispatch,
	toggleSidebar,
} from "../../context/LayoutContext";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";


const AppBar = styled(MuiAppBar, {
	shouldForwardProp: (prop) => !['open', 'kind'].includes(prop),
})(({ theme, open, kind }) => ({
	zIndex: theme.zIndex.drawer + 1,
	transition: theme.transitions.create(['width', 'margin'], {
		easing: theme.transitions.easing.sharp,
		duration: theme.transitions.duration.leavingScreen,
	}),
	...(open && {
		marginLeft: theme.sidebar[kind].width,
		width: `calc(100% - ${theme.sidebar[kind].width}px)`,
		transition: theme.transitions.create(['width', 'margin'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen,
		}),
	}),
}));

export default function AdminHeader() {
	const dispatch = useDispatch();
	const user = useSelector(AuthSelectors.user)
	const layoutState = useLayoutState();
	const layoutDispatch = useLayoutDispatch();
	const levelAlias = useSelector(LevelSelectors.matchingLevelAlias);
	const chapters = useSelector(ChapterSelectors.chapters);
	const [profileMenu, setProfileMenu] = useState(null);
	const navigation = useNavigate();

	function handleChapterSelected(_event, chapter) {
		navigation(`/app/admin/level/${levelAlias}/chapters/${chapter.id}`);
	}

	function handleLogoutClick() {
		dispatch(Actions.HEADER_LOGOUT_REQUESTED());
	}
	return (
		<AppBar position="fixed">
			<Toolbar sx={styles.toolbar}>
				<IconButton
					color="inherit"
					onClick={() => toggleSidebar(layoutDispatch)}
					sx={{
						...styles.headerMenuButton,
						...styles.headerMenuButtonCollapse,
					}}
					size="large">
					{layoutState.isSidebarOpened ? (
						<ArrowForwardIcon
							sx={{
								...styles.headerIcon,
								...styles.headerIconCollapse,
							}}
						/>
					) : (
						<MenuIcon
							sx={{
								...styles.headerIcon,
								...styles.headerIconCollapse,
							}}
						/>
					)}
				</IconButton>
				<Typography variant="h6" weight="medium" xs={styles.logotype}>
					Confidence
				</Typography>
				<Box sx={styles.grow}></Box>
				{levelAlias && <Autocomplete
					options={chapters}
					autoHighlight
					value={null}
					getOptionLabel={(option) => option.name}
					onChange={handleChapterSelected}
					renderInput={(params) => (
						<TextField
							{...params}
							sx={{width: 200, bgcolor: 'background.default'}}
							size="small"
							label="חיפוש פרק מהיר"
							variant="filled"
							inputProps={{
								...params.inputProps,
								autoComplete: 'new-password', // disable autocomplete and autofill
							}}
						/>
					)}
				/>}
				<IconButton
					aria-haspopup="true"
					color="inherit"
					sx={{ml: 2 }}
					aria-controls="profile-menu"
					onClick={e => setProfileMenu(e.currentTarget)}
					size="large">
					<AccountIcon sx={styles.headerIcon} />
				</IconButton>
				<Menu
					id="profile-menu"
					open={Boolean(profileMenu)}
					anchorEl={profileMenu}
					onClose={() => setProfileMenu(null)}
					sx={styles.headerMenu}
					// classes={{ paper: classes.profileMenu }}
					disableAutoFocusItem
				>
					<Box sx={styles.profileMenuUser}>
						<Typography variant="h4" weight="medium">
							{user.name}
						</Typography>
					</Box>
					<MenuItem
						onClick={handleLogoutClick}
						sx={{
							...styles.headerMenuItem,
							...styles.logoutMenuItem,
						}}
					>
						<ExitToAppIcon sx={styles.profileMenuIcon} /> התנתקות
					</MenuItem>
				</Menu>
			</Toolbar>
		</AppBar>
	);
}
