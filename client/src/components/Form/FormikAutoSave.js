import {useCallback, useEffect} from 'react';
import { useFormikContext } from 'formik';
import _ from 'lodash';

export default (function FormikAutoSave({ debounceMs }) {
	const formik = useFormikContext();
	const debouncedSubmit = useCallback(_.debounce(formik.submitForm, debounceMs), [debounceMs, formik.submitForm]); // eslint-disable-line react-hooks/exhaustive-deps
  
	useEffect(() => {
		debouncedSubmit();
	}, [debouncedSubmit, formik.values]);
  
	return null;
});