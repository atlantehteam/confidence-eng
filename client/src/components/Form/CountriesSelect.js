import _ from 'lodash'
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import { countriesMap, countriesCodes } from 'utils/autoComplete';
import { Autocomplete } from 'formik-mui';
import { useFormikContext } from 'formik';

export default function CountrySelect(props) {
	const {name} = props;
	const context = useFormikContext();
	const error = (context.touched[name] && context.errors[name]);
	return (
		<Autocomplete
			{...props}
			options={countriesCodes}
			autoHighlight
			disableCloseOnSelect={!!props.multiple}
			getOptionLabel={(value) => _.get(countriesMap[value], 'label', '<Invalid>')}
			renderOption={(props, value) => {
				const option = countriesMap[value] || {label: '<Invalid>'};
				return (
					(
						<Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props}>
							{!option.noFlag && 
								<img
									loading="lazy"
									width="20"
									src={`https://flagcdn.com/w20/${option.code.toLowerCase()}.png`}
									srcSet={`https://flagcdn.com/w40/${option.code.toLowerCase()}.png 2x`}
									alt=""
								/>}
							{option.label} {!option.noFlag && `(${option.code})`}
						</Box>
					)
				)
			}}
			renderInput={(params) => (
				<TextField
					{...params}
					error={!!error}
					helperText={error || props.helperText}
					label="Choose a country"
					inputProps={{
						...params.inputProps,
						autoComplete: 'new-password', // disable autocomplete and autofill
					}}
				/>
			)}
		/>
	);
}
