import React from 'react';
import {LinearProgress, Typography, Box} from '@mui/material';


const styles = {
	progress: {
		height: 10,
	},
	// colorPrimary: {
	// 	backgroundColor: theme => theme.palette.grey[theme.palette.mode === 'light' ? 300 : 700],
	// },
};

function LinearProgressWithLabel(props) {
	return (
		<Box display="flex" alignItems="center">
			<Box width="100%" mr={1}>
				<LinearProgress variant="determinate" {...props} sx={styles.progress} />
			</Box>
			<Box minWidth={35}>
				<Typography variant="body2" color="textSecondary">{`${Math.round(
					props.value,
				)}%`}</Typography>
			</Box>
		</Box>
	);
}

export default LinearProgressWithLabel;
