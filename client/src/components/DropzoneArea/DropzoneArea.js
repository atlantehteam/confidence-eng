import PropTypes from 'prop-types';
import * as React from 'react';

import {createFileFromUrl, readFile} from './dropzoneHelpers';

import DropzoneAreaBase from './DropzoneAreaBase';

const splitDropzoneAreaProps = (props) => {
	const {clearOnUnmount, initialFiles, onChange, onDelete, ...dropzoneAreaProps} = props;
	return [{clearOnUnmount, initialFiles, onChange, onDelete}, dropzoneAreaProps];
};

/**
 * This components creates an uncontrolled Material-UI Dropzone, with previews and snackbar notifications.
 *
 * It supports all props of `DropzoneAreaBase` but keeps the files state internally.
 *
 * **Note** To listen to file changes use `onChange` event handler and notice that `onDelete` returns a `File` instance instead of `FileObject`.
 */
class DropzoneArea extends React.PureComponent {
	state = {
		value: [],
	}

	componentDidMount() {
		this.loadInitialFiles();
	}

	loadInitialFiles = async() => {
		const {initialFiles} = this.props;
		try {
			const fileObjs = await Promise.all(
				initialFiles.map(async(initialFile) => {
					let file;
					if (typeof initialFile === 'string') {
						file = await createFileFromUrl(initialFile);
					} else {
						file = initialFile;
					}
					const data = await readFile(file);

					return {
						file,
						data,
					};
				})
			);

			this.props.onChange([].concat(
				this.props.value,
				fileObjs
			));

		} catch (err) {
			console.log(err);
		}
	}

	addFiles = async(newFileObjects) => {
		const {filesLimit} = this.props;
		// Update component state
		const newFiles = filesLimit <= 1 ? [newFileObjects[0]] : [].concat(this.props.value, newFileObjects)
		this.props.onChange(newFiles);
	}

	deleteFile = (removedFileObj, removedFileObjIdx, event) => {
		event.stopPropagation();

		const {onDelete} = this.props;
		const {value} = this.state;

		// Calculate remaining fileObjects array
		const remainingFileObjs = value.filter((fileObject, i) => {
			return i !== removedFileObjIdx;
		});

		// Notify removed file
		if (onDelete) {
			onDelete(removedFileObj.file);
		}

		// Update local state
		this.props.onChange(remainingFileObjs);
	}

	render() {
		const [, dropzoneAreaProps] = splitDropzoneAreaProps(this.props);
		const {value} = this.props;

		return (
			<DropzoneAreaBase
				{...dropzoneAreaProps}
				value={value}
				onAdd={this.addFiles}
				onDelete={this.deleteFile}
			/>
		);
	}
}

DropzoneArea.defaultProps = {
	clearOnUnmount: true,
	filesLimit: 3,
	initialFiles: [],
};

DropzoneArea.propTypes = {
	...DropzoneAreaBase.propTypes,
	/** Clear uploaded files when component is unmounted. */
	clearOnUnmount: PropTypes.bool,
	/** List containing File objects or URL strings.<br/>
     * **Note:** Please take care of CORS.
    */
	initialFiles: PropTypes.arrayOf(
		PropTypes.oneOfType([
			PropTypes.string,
			PropTypes.any,
		])
	),
	/** Maximum number of files that can be loaded into the dropzone. */
	filesLimit: PropTypes.number,
	/**
     * Fired when the files inside dropzone change.
     *
     * @param {File[]} loadedFiles All the files currently loaded into the dropzone.
     */
	onChange: PropTypes.func,
	/**
     * Fired when a file is deleted from the previews panel.
     *
     * @param {File} deletedFile The file that was removed.
     */
	onDelete: PropTypes.func,
};

export default DropzoneArea;