# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "3.8.0"
  constraints = "~> 3.8.0"
  hashes = [
    "h1:zAhG5lsZGX9sJUbqoaFVZbVsJnvkb+GUKyEZ+EEnW/w=",
    "zh:10b57533c2223e60bff24a74f172e36345b6bbbd32c0d0f088c0e5e87a4bd658",
    "zh:422c17906d39ccb00ea12972473c18e42d8f352f4c697feeb898209f5dc462da",
    "zh:4c3502ff8c96d7faf31a0dd7f2df2422e36e7278db004360ecd71f5af699a335",
    "zh:5900f7fbf739629afeb10017ae9880a6587310d36d0e970adb82c880d5ee9911",
    "zh:5a8dc8e6bcd7426a98b82cdc5e84c3fd9be2873cd648537e0a007b912a994df6",
    "zh:78c7d3a50901ce101ef5241ac22103f5102dd489fb91f2ceaf56b3639ad572d8",
    "zh:a1ac1388c93cde331a648cc773f94f5a3c6f1342ac5f824e5ecbd25ffdeba414",
    "zh:c650137d3dbd96ca53da4fbd71b487d4457803715aa06333540df5b0e8ad0430",
    "zh:d15d4766a2adddd108d1b1dd929d3a40ad063bee39d143ce82bca702f2951f26",
    "zh:d66e301dfa2c021cce9ead73b2565d296bcc36766b4273d01d29729848564271",
    "zh:da3a3daa0fbbb0be18c60aff6c5b694ea30cbb7cb55f7f782ec7b46658b167ab",
    "zh:e6a09b1914aa34b449fba31e626738b186c3c6d43a120aede431c791ceb42ab0",
    "zh:e9a84e74af0c9ada163cad436a6752904865e20536af9a048aa9382c12ed8f00",
    "zh:eb586af4ddcef26cef609fca80cdffc0885ec5d5465726e6d1ce98624816aaac",
  ]
}

provider "registry.terraform.io/hashicorp/external" {
  version = "2.2.2"
  hashes = [
    "h1:e7RpnZ2PbJEEPnfsg7V0FNwbfSk0/Z3FdrLsXINBmDY=",
    "zh:0b84ab0af2e28606e9c0c1289343949339221c3ab126616b831ddb5aaef5f5ca",
    "zh:10cf5c9b9524ca2e4302bf02368dc6aac29fb50aeaa6f7758cce9aa36ae87a28",
    "zh:56a016ee871c8501acb3f2ee3b51592ad7c3871a1757b098838349b17762ba6b",
    "zh:719d6ef39c50e4cffc67aa67d74d195adaf42afcf62beab132dafdb500347d39",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:7fbfc4d37435ac2f717b0316f872f558f608596b389b895fcb549f118462d327",
    "zh:8ac71408204db606ce63fe8f9aeaf1ddc7751d57d586ec421e62d440c402e955",
    "zh:a4cacdb06f114454b6ed0033add28006afa3f65a0ea7a43befe45fc82e6809fb",
    "zh:bb5ce3132b52ae32b6cc005bc9f7627b95259b9ffe556de4dad60d47d47f21f0",
    "zh:bb60d2976f125ffd232a7ccb4b3f81e7109578b23c9c6179f13a11d125dca82a",
    "zh:f9540ecd2e056d6e71b9ea5f5a5cf8f63dd5c25394b9db831083a9d4ea99b372",
    "zh:ffd998b55b8a64d4335a090b6956b4bf8855b290f7554dd38db3302de9c41809",
  ]
}

provider "registry.terraform.io/hashicorp/google" {
  version     = "4.10.0"
  constraints = "~> 4.10.0"
  hashes = [
    "h1:pP4kfF58QltprmdMmXbpgUKzlmcqrmZHf1L3DzWFLso=",
    "zh:007f591c02afb6228b81ff4d6325170664b45df56b4af74587f3230d6675f21c",
    "zh:017943b592f959998855d2c664778da03f9319c3c117dabbe61c644bf7cfb64e",
    "zh:0690b05c112e12bb5d2e69d0515ef6c0b330469e99fcb79a34790f105d988c2c",
    "zh:07029ea70670b66ffe85ea9164e7ded98ca78a89deda9aab98f4bd0b810716cf",
    "zh:0b2716d9e3fd0fb7c32f1c12636a20d0efff81fa6968e027e2b0e76f14ace71f",
    "zh:448bfc0646072bab70e932bdb91cc7c99379f771c8c668ee04895e1edc195972",
    "zh:877efd64688693a49df8591ec2487598faccbbe048787869b86c984a0943dbfb",
    "zh:99f092f81a22a6e23c42f0d0f448ae56391c252edde4d8162d759e4dd130eb93",
    "zh:c21f7e31b799b95819561e7c345a25e6f240ccc80c29de073724d0e9457ff293",
    "zh:e380365592434f2c084b05420b7c234f81a3500ded432c7499742558b2f7f9d1",
    "zh:f53b6c71b62de7912283f63064086ed7ee8bdef6142007cc0719aa1f6211b5ea",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version = "3.1.3"
  hashes = [
    "h1:nLWniS8xhb32qRQy+n4bDPjQ7YWZPVMR3v1vSrx7QyY=",
    "zh:26e07aa32e403303fc212a4367b4d67188ac965c37a9812e07acee1470687a73",
    "zh:27386f48e9c9d849fbb5a8828d461fde35e71f6b6c9fc235bc4ae8403eb9c92d",
    "zh:5f4edda4c94240297bbd9b83618fd362348cadf6bf24ea65ea0e1844d7ccedc0",
    "zh:646313a907126cd5e69f6a9fafe816e9154fccdc04541e06fed02bb3a8fa2d2e",
    "zh:7349692932a5d462f8dee1500ab60401594dddb94e9aa6bf6c4c0bd53e91bbb8",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:9034daba8d9b32b35930d168f363af04cecb153d5849a7e4a5966c97c5dc956e",
    "zh:bb81dfca59ef5f949ef39f19ea4f4de25479907abc28cdaa36d12ecd7c0a9699",
    "zh:bcf7806b99b4c248439ae02c8e21f77aff9fadbc019ce619b929eef09d1221bb",
    "zh:d708e14d169e61f326535dd08eecd3811cd4942555a6f8efabc37dbff9c6fc61",
    "zh:dc294e19a46e1cefb9e557a7b789c8dd8f319beca99b8c265181bc633dc434cc",
    "zh:f9d758ee53c55dc016dd736427b6b0c3c8eb4d0dbbc785b6a3579b0ffedd9e42",
  ]
}
