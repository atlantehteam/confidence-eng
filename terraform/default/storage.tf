data "google_storage_bucket" "shared_bucket" {
  name = "confidence-eng-shared"
}

resource "google_storage_bucket" "assets_bucket" {
  name = "${local.resource_prefix}-assets"
  location      = "EU"
  force_destroy = !local.workspace_vars["protection_enabled"]

  uniform_bucket_level_access = false
}