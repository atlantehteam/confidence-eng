locals {
  cr_env_variables = {
    TZ                        = "UTC"
    KNEX_HOST                 = "/cloudsql/${google_sql_database_instance.main.connection_name}"
    KNEX_USER                 = google_sql_user.admin.name
    KNEX_PASSWORD             = random_password.db_password.result
    KNEX_DB                   = google_sql_database.database.name
    KNEX_SSL                  = false
    ACCESS_TOKEN_SECRET       = random_password.jwt_secret.result
    RN_SERVICE_HOST           = "https://${local.cr_domain}"
    RN_APP_HOST               = "https://${local.workspace_vars.app_sub_domain}.${local.workspace_vars.base_domain}"
    RN_SMTP_ENABLED           = local.workspace_vars.smtp_enabled
    RN_SMTP_HOST              = var.RN_SMTP_HOST
    RN_SMTP_PORT              = var.RN_SMTP_PORT
    RN_SMTP_USER              = var.RN_SMTP_USER
    RN_SMTP_PASS              = var.RN_SMTP_PASS
    RN_GEO_DB_DOWNLOAD_PATH   = "/geodb/"
    RN_DEFAULT_TENANT         = local.workspace_vars.default_tenant
    RN_MIGRATION_NO_INJECTION = local.workspace_vars.disable_migration_injection
    RN_GCS_PRIVATE_BUCKET     = google_storage_bucket.assets_bucket.name
  }

  cr_domain = local.workspace_vars.api_sub_domain == "@" ? local.workspace_vars.base_domain : "${local.workspace_vars.api_sub_domain}.${local.workspace_vars.base_domain}"
}

# Find latest image tag - hack until google provide a proper way of getting this like aws_ecr_image
data "external" "latest_image_tag" {
  program = ["bash", "${path.root}/../scripts/get_image_latest_tag.sh", var.cloud_run_api_image]
}

resource "random_password" "jwt_secret" {
  length      = 64
  special     = true
  min_numeric = 6
  min_upper   = 6
  min_lower   = 6
  min_special = 6
  keepers = {
    id = local.resource_prefix
  }
}


# Create the Cloud Run service
resource "google_cloud_run_service" "run_service" {
  name     = "${local.resource_prefix}-api"
  location = "europe-west1"

  template {
    spec {
      service_account_name = google_service_account.cloud_run_server_service_account.email
      containers {
        image = "${var.cloud_run_api_image}:${data.external.latest_image_tag.result.tag}"
        dynamic "env" {
          for_each = local.cr_env_variables
          content {
            name  = env.key
            value = env.value
          }
        }
      }
    }
    metadata {
      annotations = {
        "autoscaling.knative.dev/minScale"      = local.workspace_vars.cloud_run_min_scale
        "autoscaling.knative.dev/maxScale"      = local.workspace_vars.cloud_run_max_scale
        "run.googleapis.com/cloudsql-instances" = google_sql_database_instance.main.connection_name
        "run.googleapis.com/client-name"        = "terraform"
      }
    }
  }

  autogenerate_revision_name = true
  
  traffic {
    percent         = 100
    latest_revision = true
  }

  # Waits for the Cloud Run API to be enabled
  # depends_on = [google_project_service.run_api]
}

# Allow unauthenticated users to invoke the service
resource "google_cloud_run_service_iam_member" "run_all_users" {
  service  = google_cloud_run_service.run_service.name
  location = google_cloud_run_service.run_service.location
  role     = "roles/run.invoker"
  member   = "allUsers"
}

# map domain
# NOT NEEDED FOR NOW AS BEING PROXIED IN FIREBASE HOSTING
# resource "google_cloud_run_domain_mapping" "default" {
#   location = "europe-west1"
#   name     = local.cr_domain

#   metadata {
#     namespace = var.project
#     labels    = local.common_labels
#   }

#   spec {
#     route_name = google_cloud_run_service.run_service.name
#   }
# }
