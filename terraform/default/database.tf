resource "random_id" "db_name_suffix" {
  byte_length = 4
}

resource "google_sql_database_instance" "main" {
  name                = "${local.resource_prefix}-main-${random_id.db_name_suffix.hex}"
  database_version    = "POSTGRES_13"
  deletion_protection = local.workspace_vars.protection_enabled
  settings {
    tier              = local.workspace_vars.db_instance_tier
    availability_type = local.workspace_vars.db_availability
    user_labels       = local.common_labels

    backup_configuration {
      enabled                        = true
      start_time                     = "06:00"
      point_in_time_recovery_enabled = true

      backup_retention_settings {
        retained_backups = local.workspace_vars.db_backups_count
        retention_unit   = "COUNT"
      }
    }

    maintenance_window {
      day  = 6 // saturday
      hour = 6
    }
  }
}

resource "google_sql_database" "database" {
  name     = "${local.resource_prefix}-db"
  instance = google_sql_database_instance.main.name
}

resource "random_password" "db_password" {
  length           = 16
  special          = true
  override_special = "!#$%"
  min_numeric      = 6
  min_upper        = 1
  min_lower        = 1
  min_special      = 1
  keepers = {
    id = local.resource_prefix
  }
}

resource "google_sql_user" "admin" {
  instance        = google_sql_database_instance.main.name
  name            = var.db_username
  password        = random_password.db_password.result
  deletion_policy = "ABANDON"
}

