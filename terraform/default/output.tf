output "pg_host" {
  value = google_sql_database_instance.main.connection_name
}

output "pg_password" {
  value = nonsensitive(random_password.db_password.result)
}

output "service_url" {
  value = google_cloud_run_service.run_service.status[0].url
}