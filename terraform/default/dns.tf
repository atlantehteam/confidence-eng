
data "cloudflare_zone" "main" {
  name = local.workspace_vars.base_domain
}

#cloud run - SHOUL BE DISABLED ON FIRST RUN
# NOT NEEDED FOR NOW AS BEING PROXIED IN FIREBASE HOSTING
# resource "cloudflare_record" "cloud_run" {
#   for_each = {for rr in google_cloud_run_domain_mapping.default.status[0].resource_records:  "${rr.name}_${rr.rrdata}__${rr.type}" => rr}
#   zone_id = data.cloudflare_zone.main.zone_id
#   name    = each.value.name
#   value   = trim(each.value.rrdata, ".") // trim trailing .
#   type    = each.value.type
#   ttl     = 1
#   proxied = false
# }

#firebase app dns mapping
resource "cloudflare_record" "firebase_app" {
  zone_id = data.cloudflare_zone.main.zone_id
  name    = local.workspace_vars.app_sub_domain
  value   = local.workspace_vars.firebase_domain_mapping_ip
  type    = "A"
  ttl     = 1
  proxied = true
}
