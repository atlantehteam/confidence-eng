variable "prefix" {
  default = "ec"
}

variable "project" {
  default = "confidence-eng"
}

variable "owner" {
  default = "Roi Nagar"
}

variable "db_username" {
  default = "postgres"
}

variable "cloud_run_api_image" {
  default = "europe-west1-docker.pkg.dev/confidence-eng/main/server"
}

variable "RN_SMTP_HOST" {}
variable "RN_SMTP_PORT" {}
variable "RN_SMTP_USER" {}
variable "RN_SMTP_PASS" {}

variable "env_vars" {
  default = {
    "dev" = {
      disable_migration_injection = false
      protection_enabled          = false
      db_instance_tier            = "db-f1-micro"
      db_backups_count            = "3"
      db_availability             = "ZONAL"
      default_tenant              = 999999
      base_domain                 = "confidence.me"
      app_sub_domain              = "course-dev"
      api_sub_domain              = "api-dev"
      firebase_domain_mapping_ip  = "199.36.158.100"
      smtp_enabled                = true
      cloud_run_min_scale         = 0
      cloud_run_max_scale         = 3
    }
    "prod" = {
      disable_migration_injection = false
      protection_enabled          = true
      db_instance_tier            = "db-f1-micro"
      db_backups_count            = "90"
      db_availability             = "ZONAL" // TODO: consider regional
      default_tenant              = 111111
      base_domain                 = "confidence.me"
      app_sub_domain              = "course"
      api_sub_domain              = "api"
      firebase_domain_mapping_ip  = "199.36.158.100"
      smtp_enabled                = true
      cloud_run_min_scale         = 0
      cloud_run_max_scale         = 3
    }
  }
}