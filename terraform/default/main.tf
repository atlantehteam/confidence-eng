terraform {
  backend "gcs" {
    bucket = "confidence-eng-tfstates"
    prefix = "terraform/tfstate"
  }

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.10.0"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.8.0"
    }
  }
}

provider "google" {
  project = "confidence-eng"
  region  = "europe-west1"
  zone    = "europe-west1-a"
}

provider "cloudflare" {
  // set via CLOUDFLARE_API_TOKEN env
}

locals {
  env             = terraform.workspace
  resource_prefix = "${var.prefix}-${terraform.workspace}"
  common_labels = {
    environment = terraform.workspace
    project     = var.project
    owner       = replace(lower(var.owner), " ", "-")
    managed_by  = "terraform"
  }
  workspace_vars = var.env_vars[terraform.workspace]
}
