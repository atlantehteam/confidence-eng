
resource "google_secret_manager_secret" "db_credentials" {
  secret_id = "${local.resource_prefix}-db-credentials"
  labels    = local.common_labels

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret_version" "db_credentials-version" {
  secret  = google_secret_manager_secret.db_credentials.id
  enabled = true
  secret_data = jsonencode({
    ip              = google_sql_database_instance.main.public_ip_address
    connection_name = google_sql_database_instance.main.connection_name
    user            = var.db_username,
    password        = random_password.db_password.result,
    port            = 5432
  })
}