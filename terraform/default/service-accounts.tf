resource "random_id" "role_id_suffix" {
  byte_length = 4
}
locals {
  gcp_services = ["run.googleapis.com", "secretmanager.googleapis.com", "sqladmin.googleapis.com", "iamcredentials.googleapis.com"]
}


resource "google_project_service" "run_api" {
  for_each = toset(local.gcp_services)
  service  = each.value

  disable_on_destroy = false
}

#roles
resource "google_project_iam_custom_role" "blob_signer" {
  role_id     = "${replace(local.resource_prefix, "-", "_")}_${random_id.role_id_suffix.hex}"
  title       = "Blob Signer"
  description = "Allows signing blobs"
  permissions = ["iam.serviceAccounts.signBlob"]
}

# cloud run service account
resource "google_service_account" "cloud_run_server_service_account" {
  account_id   = "${local.resource_prefix}-cloud-run-server"
  display_name = "Cloud Run Server Service Account"
}

resource "google_project_iam_member" "cr_server_sql_client_role_binding" {
  project = var.project
  role    = "roles/cloudsql.client"
  member  = "serviceAccount:${google_service_account.cloud_run_server_service_account.email}"

  condition {
    title       = "${local.resource_prefix}-sqlClient-condition"
    expression  = format("%s == \"%s\"", "resource.name", "projects/${var.project}/instances/${google_sql_database_instance.main.id}")
  }
}

resource "google_project_iam_member" "cr_server_object_viewer_role_binding" {
  project = var.project
  role    = "roles/storage.objectViewer"
  member  = "serviceAccount:${google_service_account.cloud_run_server_service_account.email}"

  condition {
    title       = "${local.resource_prefix}-objectViewer-condition"
    expression  = format(
      "(%s || %s)",
      format("(resource.type == \"storage.googleapis.com/Bucket\" && resource.name == \"%s\")", "projects/_/buckets/${data.google_storage_bucket.shared_bucket.name}"),
      format("(resource.type == \"storage.googleapis.com/Object\" && resource.name.startsWith(\"%s\"))", "projects/_/buckets/${data.google_storage_bucket.shared_bucket.name}"),
    )
  }
}

resource "google_project_iam_member" "cr_server_object_admin_role_binding" {
  project = var.project
  role    = "roles/storage.objectAdmin"
  member  = "serviceAccount:${google_service_account.cloud_run_server_service_account.email}"

  condition {
    title       = "${local.resource_prefix}-objectAdmin-condition"
    expression  = format(
      "(%s || %s)",
      format("(resource.type == \"storage.googleapis.com/Bucket\" && resource.name == \"%s\")", "projects/_/buckets/${google_storage_bucket.assets_bucket.name}"),
      format("(resource.type == \"storage.googleapis.com/Object\" && resource.name.startsWith(\"%s\"))", "projects/_/buckets/${google_storage_bucket.assets_bucket.name}"),
    )
  }
}

resource "google_project_iam_member" "cr_server_blob_signer_role_binding" {
  project = var.project
  role    = google_project_iam_custom_role.blob_signer.name
  member  = "serviceAccount:${google_service_account.cloud_run_server_service_account.email}"
}