#!/bin/bash

RAW_ENV=$(env | grep "^RN_")
TF_ENV=""
while read var; do
    splitted_var=(${var//=/ })
    TF_ENV="$TF_ENV -var '${splitted_var[0]}=${splitted_var[1]}'";
done <<< "$RAW_ENV"


echo terraform -chdir=default "$@" $TF_ENV 

