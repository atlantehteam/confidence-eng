#!/bin/bash

# Exit if any of the intermediate steps fail
set -e

TAG=$(gcloud artifacts docker images list $1 --include-tags | grep latest | awk -F',| ' '{print $5}')

# # Safely produce a JSON object containing the result value.
# # jq will ensure that the value is properly quoted
# # and escaped to produce a valid JSON string.
jq -n --arg tag "$TAG" '{"tag": $tag}'
